Rails.application.routes.draw do
  root to: 'home#index'

  resources :sessions, only: [:new, :create, :destroy]
  resources :users, only: [:index, :new, :create, :update]
  get 'items/search', to: 'items#search'
  resources :items
  resources :distinctions
  resources :categories
  resources :choices
  resources :photos
  resources :order_items, only: [:create, :update, :destroy]
  get 'orders/cart', to: 'orders#cart'
  get 'orders/customers', to: 'orders#customers'
  resources :orders, only: [:index, :show]

  post 'orders/purchase', to: 'orders#purchase'

  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'about', to: 'home#about'
  get 'order/*all', to: 'home#order'
  get 'user/*all', to: 'home#user'
  get 'users/stripe', to: 'users#stripe'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
