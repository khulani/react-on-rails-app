import React from 'react';
import axios from 'axios';
import ItemCard from '../items/ItemCard';

export default class Items extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      selected: 'ALL',
      categories: {},
      pages: 1,
      page: 0
    };
  }

  componentDidMount() {
    const { user_id, updateNotice } = this.props;
    const { page, selected } = this.state;

    if (user_id) {
      this.getItems(1, selected);
    }
    updateNotice(null);
  }

  componentDidUpdate(prevProps) {
    const { user_id } = this.props;
    const { page, selected } = this.state;

    if (user_id !== prevProps.user_id) {
      this.getItems(1, selected);
    }
  }

  getItems = (i, category) => {
    const { page, pages, selected } = this.state;
    const { user_id } = this.props;

    if (category != selected || (i != page && i > 0 && i <= pages)) {
      var url = `/items.json?user_id=${user_id}&page=${i-1}`;

      if (category != 'ALL') url += `&category=${category}`;

      axios.get(url)
        .then(res => {
          this.setState({
            items: res.data.items,
            pages: res.data.pages,
            categories: res.data.categories,
            notice: res.data.notice,
            selected: category,
            page: i
          });
          window.scrollTo(0, 0);
        })
        .catch(() => this.setState({ items: [] }));
    }
  }

  isActive = (category) => {
    const { selected } = this.state;
    return (selected == category ? ' active' : '')
  }

  getPages = () => {
    const { page, pages } = this.state;
    var pageNumbers = [];
    var start = page > 3 ? page - 3 : 1;
    var last = pages - page > 3 ? page + 3 : pages;

    for(var i=start; i <= last; i++){
      pageNumbers.push(i);
    }

    return pageNumbers;
  }

  render() {
    const { items, categories, selected, pages, page } = this.state;

    return (
      <div
        className="col-lg-8 col-xl-9"
        style={{paddingRight: '30px', paddingLeft: '30px'}}
      >
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li
              className="breadcrumb-item"
              onClick={() => { window.location.href = '/' }}
            >
              Home
            </li>
            <li className="breadcrumb-item active" aria-current="page">Items</li>
          </ol>
        </nav>
        <div className='row'>
          <div className='col-12'>
            <div
              className="categories collapsed"
              data-toggle="collapse"
              href="#collapseExample"
              role="button"
              aria-expanded="true"
              aria-controls="collapseExample"
            >
              CATEGORIES
              <div className='categories-toggle'></div>
            </div>
            <ul id='collapseExample' className="list-group collapse">
              {Object.keys(categories).map(category => (
                <li
                  key={category}
                  className={
                    "list-group-item d-flex justify-content-between align-items-center" + this.isActive(category)
                  }
                  onClick={() => this.getItems(1, category)}
                >
                  {category}
                  <span className="badge badge-secondary badge-pill">{categories[category]}</span>
                </li>
              ))}
            </ul>
          </div>
          <div className='col-12'>
            <div className='row'>
              {items.map(item => (
                selected == 'ALL' || item.categories.includes(selected) ? (
                  <ItemCard
                    key={item.id}
                    item={item}
                  />
                ) : '')
              )}
            </div>
          </div>
          <div className='col-12'>
            <nav aria-label="Page navigation example" style={{marginTop: '50px'}}>
              <ul className="pagination justify-content-center">
                <li className="page-item">
                  <a className="page-link" aria-label="Previous" onClick={() => this.getItems(page - 1, selected)}>
                    <span aria-hidden="true">&laquo;</span>
                    <span className="sr-only">Previous</span>
                  </a>
                </li>
                {this.getPages().map(i => (
                  <li key={i} className={i == page ? 'page-item active' : 'page-item'}>
                    <a className="page-link" onClick={() => this.getItems(i, selected)}>{i}</a>
                  </li>
                ))}
                <li className="page-item">
                  <a className="page-link" haria-label="Next" onClick={() => this.getItems(page + 1, selected)}>
                    <span aria-hidden="true">&raquo;</span>
                    <span className="sr-only">Next</span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    );
  }
}
