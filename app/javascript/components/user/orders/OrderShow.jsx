import React from 'react';
import axios from 'axios';
import OrderItem from '../../order/OrderItem'

export default class OrderShow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      order: {
        order_items: {},
        total: 0,
        uuid: '',
        address: {},
        payment: {}
      }
    };
  }

  componentDidMount() {
    const { updateNotice, match } = this.props;

    updateNotice(null);
    axios.get(`/orders/${match.params.id}.json`)
      .then(res => {
        this.setState({ order: res.data });
      })
      .catch(() => console.log('unable to get order_items'));
  }

  render() {
    const { order } = this.state;
    const { history } = this.props;

    return (
      <div
        className="col-lg-8 col-xl-9 mb-5"
        style={{paddingRight: '30px', paddingLeft: '30px'}}
      >
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item" onClick={() => { window.location.href = '/' }}>
              Home
            </li>
            <li className="breadcrumb-item" onClick={() => history.push('/orders')}>
              Orders
            </li>
            <li className="breadcrumb-item active" aria-current="page">{order.uuid}</li>
          </ol>
        </nav>
        <div className='row'>
          <div className='col-8 mb-1' style={{fontSize: '2rem'}}>
            {order.uuid}
          </div>
          <div className='col-4 text-right' style={{paddingTop: '14px'}} >
            <i>{order.payment.created_at}</i>
          </div>
          <div className='col-12 col-sm-5'>
            <div className='row cart-header checkout'>
              <div className='col-12'>Order Details</div>
            </div>
            <div className='row'>
              <div className='col-12'>{order.payment.card_type}</div>
              <div className='col-12  '>
                xxxx-xxxx-xxxx-{order.payment.card}
              </div>
              <div className='col-12'>{order.email_address}</div>
            </div>
          </div>
          <div className='col-2'></div>
          <div className='col-12 col-sm-5 mb-2'>
            <div className='row cart-header checkout'>
              <div className='col-12'>Shipping Address</div>
            </div>
            <div className='row'>
              <div className='col-12'>{order.address.full_name}</div>
              <div className='col-12'>{order.address.street1}</div>
              <div className='col-12'>
                {order.address.city}, {order.address.state} {order.address.zip_code}
              </div>
            </div>
          </div>
        </div>
        <div className='row cart-header'>
          <div className='col-3 col-md-2'></div>
          <div className='col-9 col-md-5'>ITEM</div>
          <div
            className='col-3 d-block d-sm-block d-md-none text-right'
            style={{paddingRight: 0}}
          >
            &
          </div>
          <div className='col-5 col-md-3'>PRICE</div>
          <div className='col-4 col-md-2'>QTY</div>
        </div>
        {
          Object.keys(order.order_items).map(key => (
            <OrderItem key={key} order_item={order.order_items[key]} />
          ))
        }
        <div className="row mt-3 mb-5">
          <div className='col-1 col-md-2'></div>
          <div className='col-7 col-md-5'><h4><b>TOTAL:</b></h4></div>
          <div className='col-4 col-md-5'>
            <h4><b>${Number(order.total).toFixed(2)}</b></h4>
          </div>
        </div>
      </div>
    );
  }
}
