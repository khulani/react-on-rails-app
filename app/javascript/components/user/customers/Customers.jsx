import React from 'react';
import axios from 'axios';

export default class Customers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: []
    };
  }

  componentDidMount() {
    const { updateNotice } = this.props;
    const { page, selected } = this.state;

    updateNotice(null);
    axios.get('/orders.json?customer=true')
      .then(res => {
        this.setState({ orders: res.data.orders });
      })
      .catch(() => console.log('unable to get orders'));
  }



  render() {
    const { orders } = this.state;
    const { history } = this.props;

    return (
      <div
        className="col-lg-8 col-xl-9 mb-5"
        style={{paddingRight: '30px', paddingLeft: '30px'}}
      >
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li
              className="breadcrumb-item"
              onClick={() => { window.location.href = '/' }}
            >
              Home
            </li>
            <li className="breadcrumb-item active" aria-current="page">Customers</li>
          </ol>
        </nav>
        <div className='row cart-header'>
          <div className='col-12 d-sm-block d-md-none'>ORDER INFO</div>
          <div className='col-md-4 d-none d-sm-none d-md-block'>ORDER ID</div>
          <div className='col-md-3 d-none d-sm-none d-md-block'>STATUS</div>
          <div className='col-md-2 d-none d-sm-none d-md-block'>TOTAL</div>
          <div className='col-md-3 d-none d-sm-none d-md-block'>DATE</div>
        </div>
        { orders.length ? (
          orders.map(order => (
            <div key={order.id} className='row'>
              <div
                className='col-sm-12 col-md-4 order-uuid'
                onClick={() => history.push(`/customers/${order.id}`)}
              >
                <b>{order.uuid}</b>
              </div>
              <div className='col-sm-12 col-md-3'>{order.status}</div>
              <div className='col-sm-12 col-md-2'>${Number(order.total).toFixed(2)}</div>
              <div className='col-sm-12 col-md-3'>{order.payment.created_at}</div>
              <div className='col-12 mb-2 d-sm-block d-md-none'></div>
            </div>
          ))
        ) : (
          <div className="row mt-3">
            <div className='col-12 text-center'><i>EMPTY</i></div>
          </div>
        ) }
      </div>
    );
  }
}
