import React from 'react';
import axios from 'axios';

export default class Avatar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: ''
    };
  }

  uploadPhoto = e => {
    const { user, updateUser, updateNotice } = this.props;
    const fd = new FormData();
    const file =  e.target.files[0];

    if (file.size > 1000000) {
      this.setState({error: 'File size limit is 1MB'})
    } else {
      this.setState({error: false})
      fd.append('user[avatar]', file, new Date().getTime().toString() + '_' + file.name);

      axios.put(`/users/${user.id}`, fd)
        .then(res => {
          updateUser({avatar: res.data.avatar});
          updateNotice({message: 'Photo updated.', type: 'success', shown: true});
        })
        .catch(() => this.setState({ errors: 'Unable to update photo' }));
    }
  }

  render() {
    const { error } = this.state;
    const { user } = this.props;

    return (
      <div className="profile text-center">
        <input
          type="file"
          style={{display: 'none'}}
          className="form-control"
          onChange={this.uploadPhoto}
          ref={inputPhoto => this.inputPhoto = inputPhoto}
        />
        <a
          className="d-inline-block avatar-link mb-3"
          onClick={() => this.inputPhoto.click()}
        >
          { user.avatar ? (
            <img src={user.avatar} className="img-fluid rounded-circle" />
          ) : (
            <div className="avatar-empty" />
          ) }
        </a>
        { error ? (
          <div className="alert alert-danger" style={{marginTop: '5px'}} role="alert">
            {error}
          </div>
        ) : null }
        <h5>{user.first_name} {user.last_name}</h5>
        <p className="text-muted text-sm mb-0"></p>
      </div>
    );
  }
}
