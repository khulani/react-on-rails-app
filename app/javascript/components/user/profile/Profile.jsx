import React from 'react';
import axios from 'axios';

export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: this.props.user,
      password: { old: '', new: '', match: '' }
    };
  }

  componentDidMount() {
    const { notice, updateNotice } = this.props;
    if (!notice) return;

    if (notice.shown) {
      this.props.updateNotice(null);
    } else {
      this.props.updateNotice({...notice, shown: true});
    }
  }

  componentDidUpdate(prevProps) {
    const { user } = this.props;

    if (user !== prevProps.user) {
      this.setState({
        user
      });
    }
  }

  handleInputChange = (event) => {
    const { user } = this.state;
    const value = event.target.value;
    const name = event.target.name;

    this.setState({
      user: { ...user, [name]: value }
    });
  }

  handlePasswordInfo =  (event) => {
    const { password } = this.state;
    let updatedPassword = {
      ...password,
      [event.target.name]: event.target.value
    };

    if (updatedPassword.match && updatedPassword.new != updatedPassword.match) {
      updatedPassword.error =  'Password mismatched.';
    } else {
      updatedPassword.error = '';
    }

    this.setState({
      password: { ...updatedPassword }
    });
  }

  stripeConnectUrl = () => {
    const { user } = this.state;

    let url = 'https://connect.stripe.com/oauth/authorize';
    // let url = 'https://connect.stripe.com/express/oauth/authorize';
    let params = '?response_type=code&scope=read_write'
    // let client_id = '&client_id=ca_EG48EEgD3DraQoKU8xOIGPI5j6OtwscD';
    let client_id = '&client_id=ca_CovfTAaXIJhWGKX3EO2RbqJdms5SPP9i';
    // let redirect_uri ='&redirect_uri=localhost:3000/test'

    return url + params + client_id;
  }

  disconnectStripe = () => {
    const { user } = this.state;

    axios.put(`/users/${user.id}.json`, { user: { stripe_user_id: '' } })
      .then(res => {
        this.setState({
          user: res.data
        });
        this.props.updateUser(res.data);
        this.props.updateNotice({
          message: 'Stripe disconnected.',
          type: 'success',
          shown: true
        });
        window.scrollTo(0, 0);
      })
      .catch(error => {
        this.props.updateNotice({
          message: 'Unable to disconnect Stripe.',
          type: 'warning',
          shown: true
        });
        window.scrollTo(0, 0);
      })
  }

  updateProfile = (e) => {
    e.preventDefault();
    const { user } = this.state;
    const { updateNotice } = this.props;

    axios.put(`/users/${user.id}.json`, { user: user })
      .then(res => {
        updateNotice({ message: 'Profile updated.', type: 'success' });
        window.scrollTo(0, 0);
        this.props.updateUser(user);
      })
      .catch(error => {
        updateNotice({ message: 'Unable to update profile.', type: 'danger' });
        window.scrollTo(0, 0);
      })
  }

  updatePassword = (e) => {
    e.preventDefault();
    const { user, password } = this.state;
    const { updateNotice } = this.props;

    if (!(password.old && password.new && password.match)) {
      this.setState({
        password: { ...password, error: 'Missing info.' }
      })
    } else if (!password.error) {
      axios.put(
        `/users/${user.id}.json`,
        {
          user: {
            old_password: password.old,
            password: password.new,
            password_confirmation: password.match
          }
        }
      )
      .then(res => {
        updateNotice({ message: 'Password updated.', type: 'success' });
        this.setState({
          password: { old: '', new: '', match: '' }
        });
        window.scrollTo(0, 0);
      })
      .catch(error => {
        updateNotice({ message: error.response.data.message, type: 'danger' });
        window.scrollTo(0, 0);
      })
    }
  }

  render() {
    const { user, password } = this.state;

    return (
      <div
        className="col-lg-8 col-xl-9"
        style={{paddingRight: '30px', paddingLeft: '30px'}}
      >
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li
              className="breadcrumb-item"
              onClick={() => { window.location.href = '/' }}
            >
              Home
            </li>
            <li className="breadcrumb-item active" aria-current="page">Profile</li>
          </ol>
        </nav>

        <div className="row cart-header checkout">
          <div className="col-12">UPDATE YOUR PROFILE</div>
        </div>
        <form>
          <div className="row">
            <div className="col-sm-6">
              <div className="form-group">
                <label className="form-label">First Name</label>
                <input
                  type="text"
                  name='first_name'
                  className="form-control"
                  value={user.first_name}
                  onChange={this.handleInputChange}
                />
              </div>
            </div>
            <div className="col-sm-6">
              <div className="form-group">
                <label className="form-label">Last Name</label>
                <input
                  type="text"
                  name='last_name'
                  className="form-control"
                  value={user.last_name}
                  onChange={this.handleInputChange}
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <div className="form-group">
                <label className="form-label">Email</label>
                <input
                  type="text"
                  name='email'
                  className="form-control"
                  value={user.email}
                  onChange={this.handleInputChange}
                />
              </div>
            </div>
          </div>
          <div className="text-center mt-4 mb-5">
            <button className="btn btn-outline-dark" onClick={this.updateProfile}>
              Save changes
            </button>
          </div>
        </form>

        <div className="row cart-header checkout">
          <div className="col-12">CONNECT YOUR STRIPE ACCOUNT</div>
        </div>
        <form>
          <div className='row mb-5'>
            <div className='col-12'>
              <div className="form-group">
                <label className="form-label text-muted">
                  <i>
                    {'Status: '}{ user.stripe_user_id ? 'Connected.' : 'Not connected.' }
                  </i>
                </label>
                <a href={this.stripeConnectUrl()}>
                  <div className='stripe-connect' />
                </a>
              </div>
            </div>
            { user.stripe_user_id ? (
              <div className='col-12'>
                <div onClick={this.disconnectStripe} className='stripe-disconnect'>
                  Disconnect your account
                </div>
              </div>
            ) : null }
          </div>
        </form>

        <div className="row cart-header checkout">
          <div className="col-12">CHANGE YOUR PASSWORD</div>
        </div>
        <form>
          { password.error ? (
            <div className='row'>
              <div className='col-12 text-center alert alert-danger' role="alert">
                {password.error}
              </div>
            </div>
          ) : null }
          <div className="row">
            <div className="col-sm-6">
              <div className="form-group">
                <label className="form-label">Old password</label>
                <input
                  type="password"
                  name='old'
                  className="form-control"
                  value={password.old}
                  onChange={this.handlePasswordInfo}
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <div className="form-group">
                <label className="form-label">New password</label>
                <input
                  type="password"
                  name='new'
                  className="form-control"
                  value={password.new}
                  onChange={this.handlePasswordInfo}
                />
              </div>
            </div>
            <div className="col-sm-6">
              <div className="form-group">
                <label className="form-label">Retype new password</label>
                <input
                  type="password"
                  name='match'
                  className="form-control"
                  value={password.match}
                  onChange={this.handlePasswordInfo}
                />
              </div>
            </div>
          </div>
          <div className="text-center mt-4 mb-4">
            <button
              className="btn btn-outline-dark"
              onClick={this.updatePassword}
            >
              Change password
            </button>
          </div>
        </form>
      </div>
    );
  }
}
