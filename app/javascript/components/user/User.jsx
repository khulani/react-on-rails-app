import React from 'react';
import axios from 'axios';
import { BrowserRouter, Route, Switch, NavLink } from 'react-router-dom';
import Profile from './profile/Profile';
import Avatar from './profile/Avatar';
import Items from './Items';
import Orders from './orders/Orders';
import OrderShow from './orders/OrderShow';
import Customers from './customers/Customers';
import Customer from './customers/Customer';

export default class Order extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      notice: this.props.notice,
      user: { email: '', first_name: '', last_name: '' }
    }
  }

  componentDidMount() {
    axios.get('/users.json')
      .then(res => {
        this.setState({
          user: res.data
        });
        // window.scrollTo(0, 0);
      })
      .catch(error => console.log('users.json error', error));
  }

  updateUser = (userUpdated) => {
    const { user } = this.state;
    this.setState({ user: { ...user, ...userUpdated } });
  }

  updateNotice = (notice) => {
    this.setState({ notice })
  }

  render() {
    const { user, notice } = this.state;

    return (
      <BrowserRouter basename='/user'>
        <div className="row">
          { notice ? (
            <div className={'col-12 text-center alert alert-' + notice.type} role="alert">
              {notice.message}
            </div>
          ) : '' }
          <div className="col-xl-3 col-lg-4 mb-3">
            <div className="customer-sidebar card border-0">
              <Avatar
                user={user}
                updateUser={this.updateUser}
                updateNotice={this.updateNotice}
              />
              <nav className="list-group">
                <NavLink to="/profile/" className="list-group-item d-flex justify-content-between align-items-center">
                  Profile
                </NavLink>
                <NavLink to="/items/" className="list-group-item d-flex justify-content-between align-items-center">
                  Items
                </NavLink>
                <NavLink to="/orders/" className="list-group-item d-flex justify-content-between align-items-center">
                  Orders
                </NavLink>
                <NavLink to="/customers/" className="list-group-item d-flex justify-content-between align-items-center">
                  Customers
                </NavLink>
                <a
                  href='/logout'
                  className="list-group-item d-flex justify-content-between align-items-center"
                  style={{color: 'inherit'}}
                >
                  Log out
                </a>
              </nav>
            </div>
          </div>

          <Switch>
            <Route path='/profile'
              render={ props => (
                <Profile
                  {...props}
                  notice={notice}
                  user={user}
                  updateUser={this.updateUser}
                  updateNotice={this.updateNotice}
                />
              ) }
            />
            <Route path='/items'
              render={ props => (
                <Items user_id={user.id} updateNotice={this.updateNotice} />
              ) }
            />
            <Route exact path='/orders'
              render={ props => (
                <Orders {...props} updateNotice={this.updateNotice} />
              ) }
            />
            <Route path='/orders/:id'
              render={ props => (
                <OrderShow {...props} updateNotice={this.updateNotice} />
              ) }
            />
            <Route exact path='/customers'
              render={ props => (
                <Customers {...props} updateNotice={this.updateNotice} />
              ) }
            />
            <Route exact path='/customers/:id'
              render={ props => (
                <Customer {...props} updateNotice={this.updateNotice} />
              ) }
            />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}
