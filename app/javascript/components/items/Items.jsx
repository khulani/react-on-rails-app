import React from 'react';
import axios from 'axios';
import ItemCard from './ItemCard';

export default class Items extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      selected: 'ALL',
      query: '',
      categories: {},
      pages: 1,
      page: 1,
      notice: this.props.notice,
      loading: false
    };
  }

  componentDidMount() {
    axios.get('/items.json')
      .then(res => {
        this.setState({
          items: res.data.items,
          pages: res.data.pages,
          categories: res.data.categories,
        });
        window.scrollTo(0, 0);
      })
      .catch(() => this.setState({ items: [] }));
  }

  getItems = (i, category, search) => {
    const { page, pages, selected, query, loading } = this.state;
    console.log('getItems', search, query);
    if (category != selected || loading ||
        (i != page && i > 0 && i <= pages)) {
      var url = '/items.json?page=' + (i - 1);

      if(category != 'ALL') url += '&category=' + category;
      if(query) url += '&query=' + search;

      axios.get(url)
        .then(res => {
          this.setState({
            items: res.data.items,
            pages: res.data.pages,
            categories: res.data.categories,
            notice: res.data.notice,
            selected: category,
            page: i,
            loading: false
          });
          window.scrollTo(0, 0);
        })
        .catch(() => this.setState({ items: [], loading: false }));
    }
  }

  isActive = (category) => {
    const { selected } = this.state;
    return (selected == category ? ' active' : '')
  }

  getPages = () => {
    const { page, pages } = this.state;
    var pageNumbers = [];
    var start = page > 3 ? page - 3 : 1;
    var last = pages - page > 3 ? page + 3 : pages;

    for(var i=start; i <= last; i++){
      pageNumbers.push(i);
    }

    return pageNumbers;
  }

  onChange = (e) => {
    const { page, selected } = this.state;
    const { value } = e.target;
    console.log('searching...', value);
    if(this.timeout) clearTimeout(this.timeout);
    this.setState({ loading: true, query: value });

    this.timeout = setTimeout(() => {
      this.getItems(page, selected, value);
    }, 300);
  }

  handleSubmit = (e) => {
    e.preventDefault();
  }

  render() {
    const {
      items,
      categories,
      selected,
      pages,
      page,
      notice,
      query,
      loading
    } = this.state;

    return (
      <div className='row'>
        { notice ? (
          <div className={'col-12 text-center alert alert-' + notice.type} role="alert">
            {notice.message}
          </div>
        ) : '' }
        <div className='col-4 col-sm-4 col-lg-5 col-xl-5'></div>
        <div className='col-4 col-sm-4 col-lg-3 col-xl-3'>
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">Home</li>
              <li className="breadcrumb-item active" aria-current="page">Items</li>
            </ol>
          </nav>
        </div>
        <div className='col-4 col-sm-4 col-lg-4 col-xl-4'></div>
        <div className='col-xl-3 col-lg-4'>
          <div
            className="categories"
            data-toggle="collapse"
            href="#collapseExample"
            role="button"
            aria-expanded="true"
            aria-controls="collapseExample"
          >
            CATEGORIES
            <div className='categories-toggle'></div>
          </div>
          <ul id='collapseExample' className="list-group collapse show">
            { Object.keys(categories).map(category => (
              <li
                key={category}
                className={
                  "list-group-item d-flex justify-content-between align-items-center" + this.isActive(category)
                }
                onClick={() => this.getItems(1, category, query)}
              >
                {category}
                <span className="badge badge-secondary badge-pill">{categories[category]}</span>
              </li>
            )) }
          </ul>
        </div>
        <div className='col-xl-9 col-lg-8'>
          <div className='row mt-3'>
            <div className='col-1'></div>
            <div className='col-10'>
              <form className="form" onSubmit={this.handleSubmit}>
                <input
                  className="form-control"
                  placeholder="Search items..."
                  onChange={this.onChange}
                  value={query}
                />
              </form>
            </div>
            <div className='col-1'></div>
          </div>
          { !loading && items.length ? (
            <div className='row'>
              { items.map(item => (
                <ItemCard
                  key={item.id}
                  item={item}
                />
              )) }
            </div>
          ) : (
            <div className='row mt-3'>
              <div className='col-md-2'></div>
              <div className='col-md-10 d-none d-sm-none d-md-none d-lg-block'>
                { loading ? 'Loading...' : 'No results.' }
              </div>
              <div className='col-12 col-sm-12 d-block d-sm-block d-md-block d-lg-none text-center'>
                { loading ? 'Loading...' : 'No results.' }
              </div>
            </div>
          ) }

        </div>
        <div className='col-12'>
          <nav aria-label="Page navigation example" style={{marginTop: '50px'}}>
            <ul className="pagination justify-content-center">
              <li className="page-item">
                <a className="page-link" aria-label="Previous" onClick={() => this.getItems(page - 1, selected, query)}>
                  <span aria-hidden="true">&laquo;</span>
                  <span className="sr-only">Previous</span>
                </a>
              </li>
              { this.getPages().map(i => (
                <li key={i} className={i == page ? 'page-item active' : 'page-item'}>
                  <a className="page-link" onClick={() => this.getItems(i, selected)}>{i}</a>
                </li>
              )) }
              <li className="page-item">
                <a className="page-link" haria-label="Next" onClick={() => this.getItems(page + 1, selected, query)}>
                  <span aria-hidden="true">&raquo;</span>
                  <span className="sr-only">Next</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}
