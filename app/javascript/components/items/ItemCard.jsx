import React from 'react';

export default class ItemCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item || []
    };
  }

  render() {
    const { item } = this.state;
    return (
      <div className='col-xl-4 col-sm-6'>
        <div className="item-card text-center">
          <a href={'/items/' + item.id}>
            <div
              className="item-image-container"
              style={
                item.photos[0] ? (
                  {
                    backgroundImage: 'url('+ item.photos[0].photo_url +')'
                  }
                ) : (
                  {
                    backgroundColor: '#ececec'
                  }
                )
              }
            />
          </a>
          <div>
            <a href={'/items/' + item.id}>
              <span className='item-card-title'>{item.title}</span>
            </a>
          </div>
          <div className='item-seller'>{'by ' + item.user}</div>
          <div>
            <span className='item-price'>{'$' + Number(item.sale_price || item.price).toFixed(2)}</span>
            { item.sale_price ? <span className='item-original-price'>{'$' + Number(item.price).toFixed(2)}</span> : ''}
          </div>
        </div>
      </div>
    );
  }
}
