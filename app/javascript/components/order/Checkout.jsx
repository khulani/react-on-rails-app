import React from 'react';
import { injectStripe, CardElement } from 'react-stripe-elements';

class Checkout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      errors: {
        address: '',
        email: '',
        card: ''
      },
      address: this.props.customer.address,
      email: this.props.customer.email,
      card: false
    };
  }

  handleCheckout = () => {
    const { history, updateCustomer } = this.props;
    const { address, email } = this.state;

    if (this.validateForm()) {
      this.props.stripe.createToken({name: 'card'}).then(({token}) => {
        updateCustomer({
          email,
          address,
          payment: token
        });
        history.push('/review')
      });
    }

  }

  validateForm = () => {
    const { address, email, card, errors } = this.state;
    let errorsUpdated = { ...errors };

    if (address.full_name && address.street1 && address.city && address.state &&
      address.zip_code && email) {
        errorsUpdated.address = '';
    } else {
      errorsUpdated.address = 'Missing shipping address info.';
    }

    if (!errors.card && !card) {
      errorsUpdated.card = 'Missing credit card info.';
    }

    this.setState({ errors: errorsUpdated })

    return !(errorsUpdated.address || errorsUpdated.email || errorsUpdated.card);
  }

  handleCardInput = (event) => {
    const { errors } = this.state;
    if (event.error) {
      this.setState({ errors: { ...errors, card: event.error.message } });
    } else {
      this.setState({ errors: { ...errors, card: '' }, card: event.complete });
    }
  }

  handleAddressInput = (event) => {
    const { address } = this.state;
    const value = event.target.value;
    const name = event.target.name;

    this.setState({
      address: { ...address, [name]: value }
    });
  }

  handleEmailInput = (event) => {
    this.setState({ email: event.target.value })
  }

  render() {
    const { address, email, errors } = this.state;
    const { history } = this.props;

    return (
      <div>
        <div className='row'>
          <div className='col-md-12 ml-4'>
            <h1>Checkout</h1>
          </div>
        </div>
        <div className='row cart-header checkout'>
          <div className='col-md-12'>Credit Card</div>
        </div>
        <div className='row'>
          <div className="form-group col-md-6">
            { errors.card ? (
              <div className="alert alert-danger" role="alert">
              { errors.card }
              </div>
            ) : null }
            <CardElement
              className='mt-3'
              style={{base: {fontSize: '18px'}}}
              onChange={this.handleCardInput}
            />
          </div>
        </div>
        <div className='row cart-header checkout'>
          <div className='col-md-12'>Shipping Address</div>
        </div>
        <div className="row">
          { errors.address ? (
            <div className="form-group col-md-12">
              <div className="alert alert-danger" role="alert">
              { errors.address }
              </div>
            </div>
          ) : null }
          <div className="form-group col-md-6">
            <label className="form-label">Full Name</label>
            <input
              name='full_name'
              type="text"
              placeholder="John Smith"
              className="form-control"
              value={address.full_name}
              onChange={this.handleAddressInput}
            />
          </div>
          <div className="form-group col-md-6">
            <label className="form-label">Email Address</label>
            <input
              type="text"
              placeholder="john.smith@gmail.com"
              className="form-control"
              value={email}
              onChange={this.handleEmailInput}
            />
          </div>
          <div className="form-group col-md-6">
            <label className="form-label">Street</label>
            <input
              name='street1'
              type="text"
              placeholder="123 Main St."
              className="form-control"
              value={address.street1}
              onChange={this.handleAddressInput}
            />
          </div>
          <div className="form-group col-md-6">
            <label className="form-label">City</label>
            <input
              name='city'
              type="text"
              placeholder="City"
              className="form-control"
              value={address.city}
              onChange={this.handleAddressInput}
            />
          </div>
          <div className="form-group col-md-6">
            <label className="form-label">ZIP</label>
            <input
              name='zip_code'
              type="text"
              placeholder="Postal code"
              className="form-control"
              value={address.zip_code}
              onChange={this.handleAddressInput}
            />
          </div>
          <div className="form-group col-md-6">
            <label className="form-label">State</label>
            <input
              name='state'
              type="text"
              placeholder="State"
              className="form-control"
              value={address.state}
              onChange={this.handleAddressInput}
              />
          </div>
        </div>
        <div className='row mt-4 mb-5'>
          <div className='col-6 text-right'>
            <button
              type="button"
              className="btn btn-outline-secondary"
              onClick={() => history.push('/cart')}
            >
              VIEW CART
            </button>
          </div>
          <div className='col-6 text-left'>
            <button
              type="button"
              className="btn btn-secondary"
              onClick={this.handleCheckout}
            >
              REVIEW
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default injectStripe(Checkout);
