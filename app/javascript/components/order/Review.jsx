import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

class Review extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error: '',
      disabled: ''
    };
  }

  getTotal = () => {
    const { order_items } = this.props;

    return (
      Object.values(order_items).reduce((total, order) => (
        total + Number(order.item.price) * Number(order.qty)
      ), 0)
    );
  }

  placeOrder = () => {
    const { disabled } = this.state;
    const { history, customer, updateCustomer } = this.props;

    this.setState({ disabled: 'disabled' });

    if (!disabled) {
      axios.post('/orders/purchase.json', {
        address: customer.address,
        payment: {
          card: customer.payment.card.last4,
          card_type: customer.payment.card.brand,
          token: customer.payment.id
        },
        email: customer.email
      })
        .then(res => {
          updateCustomer({ order_id: res.data.order_id });
          history.push('/confirm');
        })
        .catch(() => {
          window.scrollTo(0, 0);
          this.setState({
            error: 'Unable to place order.'
          })
        });
    }
  }

  render() {
    const { error, disabled } = this.state;
    const { order_items, history, customer } = this.props;
    const orderCount = Object.keys(order_items).length;
    console.log('customer', customer);

    return (
      <div>
        <div className='row'>
          <div className='col-md-12 ml-4'>
            <h1>Review</h1>
          </div>
        </div>
        { error ? (
          <div className='row'>
            <div className="col-12">
              <div className="alert alert-danger" role="alert">
              { error }
              </div>
            </div>
          </div>
        ) : null }
        <div className='row mb-3'>
          <div className='col-md-3'>
            <div className='row cart-header checkout'>
              <div className='col-12' >Shipping Address</div>
            </div>
            <div className='row'>
              <div className='col-12'>{customer.address.full_name}</div>
              <div className='col-12'>{customer.address.street1}</div>
              <div className='col-12'>
                {customer.address.city}, {customer.address.state} {customer.address.zip_code}
              </div>
            </div>
            <div className='row cart-header checkout'>
              <div className='col-12'>Credit Card</div>
            </div>
            <div className='row'>
              <div className='col-12'>{customer.payment.card.brand}</div>
              <div className='col-12'>xxxx-xxxx-xxxx-{customer.payment.card.last4}</div>
              <div className='col-12'>
                {customer.payment.card.exp_month} / {customer.payment.card.exp_year}
              </div>
            </div>
          </div>
          <div className='col-md-1'></div>
          <div className='col-md-8'>
            <div className='row cart-header'>
              <div className='col-md-2 d-none d-sm-none d-md-block'></div>
              <div className='col-7 col-md-5'>ITEM</div>
              <div className='col-3 col-md-2'>PRICE</div>
              <div className='col-2 col-md-3'>QTY</div>
            </div>
            {
              Object.keys(order_items).map(key => (
                <div key={key} className='row'>
                  <div className='col-md-2 d-none d-sm-none d-md-block'></div>
                  <div className='col-7 col-md-5'>{order_items[key].item.title}</div>
                  <div className='col-3 col-md-2'>{order_items[key].item.price}</div>
                  <div className='col-2 col-md-3'>{order_items[key].qty}</div>
                </div>
              ))
            }
            <div className="row mt-3">
              <div className='col-1 col-md-2'></div>
              <div className='col-7 col-md-5'><h4><b>TOTAL:</b></h4></div>
              <div className='col-4 col-md-5'>
                <h4><b>${Number(this.getTotal()).toFixed(2)}</b></h4>
              </div>
            </div>
          </div>
        </div>
        <div className='row mt-5 mb-5'>
          <div className='col-6 text-right'>
            <button
              type="button"
              className="btn btn-outline-secondary"
              onClick={() => history.push('/checkout')}
            >
              UPDATE INFO
            </button>
          </div>
          <div className='col-6 text-left'>
            <button
              type="button"
              className={`btn btn-secondary ${disabled}`}
              onClick={this.placeOrder}
            >
              PLACE ORDER
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('mapStateToProps', state);
  return {
    order_items: state.cart.order.order_items
  };
}

export default connect(mapStateToProps)(Review);
