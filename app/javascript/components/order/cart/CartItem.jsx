import React from 'react';

export default class CartItem extends React.Component {
  redirectToItem = (id) => {
    const { order } = this.props;

    window.location.href = '/items/' + order.item.id;
  }

  updateQty = (e) => {
    const { order, addToCart } = this.props;
    let qty = e.nativeEvent.target.value;

    addToCart(Object.assign(order, { qty }));
  }

  render() {
    const { order, removeFromCart } = this.props;
    const qtyChoices = [...Array(10).keys()];

    return (
      <div className='row cart-item-row'>
        { order.item.photo_url ? (
          <div
            className="col-3 order-1 col-md-2 cart-item-image"
            onClick={this.redirectToItem}
            style={
              {
                backgroundImage: `url(${order.item.photo_url})`
              }
            }
          />
        ) : (
          <div
            className="col-3 order-1 col-md-2 cart-item-image"
            onClick={this.redirectToItem}
            style={
              {
                backgroundColor: '#ececec'
              }
            }
          />
        ) }
        <div className='col-8 order-2 col-md-5'>
          <h3 className="card-title" onClick={this.redirectToItem}>
            {order.item.title}
          </h3>
          <p className="card-text">
            { order.selection ? Object.keys(order.selection).map(key => (
              `${key}: ${order.selection[key]}`
            )).join(', ') : null }
          </p>
        </div>
        <div className='col-5 order-5 order-md-3 col-md-2 cart-item-price'>
          ${Number(order.item.price).toFixed(2)}
        </div>
        <div className='col-4 order-6 order-md-4 col-md-2 cart-item-qty'>
          <select
            className='form-control ml-1 item-qty-select'
            onChange={this.updateQty}
            value={order.qty}
          >
            { qtyChoices.map((_, i) => (
              <option key={i} value={i + 1}>
                {i + 1}
              </option>
            )) }
          </select>
        </div>
        <div
          className="col-1 order-3 order-md-5 col-md-1 close"
          onClick={() => removeFromCart(order.item.id)}
        >
          <span className="tag-close" aria-hidden="true">&times;</span>
        </div>
        <div className='col-3 order-4 order-md-6'></div>
      </div>
    );
  }
}
