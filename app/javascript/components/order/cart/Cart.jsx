import React from 'react';
import { connect } from 'react-redux';
import { addToCart, removeFromCart } from '../../../store/actions/CartActions';
import CartItem from './CartItem';

class Cart extends React.Component {

  getTotal = () => {
    const { order_items } = this.props;

    return (
      Object.values(order_items).reduce((total, order) => (
        total + Number(order.item.price) * Number(order.qty)
      ), 0)
    );
  }

  render() {
    const { order_items, addToCart, removeFromCart, history } = this.props;
    const orderCount = Object.keys(order_items).length;
    const checkout = orderCount ? '' : 'disabled';

    return (
      <div>
        <div className='row'>
          <div className='col-md-12 ml-4'>
            <h1>Shopping Cart</h1>
          </div>
        </div>
        <div className='row cart-header'>
          <div className='col-3 col-md-2'></div>
          <div className='col-9 col-md-5'>ITEM</div>
          <div
            className='col-3 d-block d-sm-block d-md-none text-right'
            style={{paddingRight: 0}}
          >
            &
          </div>
          <div className='col-5 col-md-2'>PRICE</div>
          <div className='col-4 col-md-3'>QTY</div>
        </div>
        {
          orderCount ? (
            Object.keys(order_items).map(key => (
              <CartItem
                key={key}
                order={order_items[key]}
                addToCart={addToCart}
                removeFromCart={removeFromCart}
              />
            ))
          ) : (
            <div className="row mt-3">
              <div className='col-12 text-center'><i>EMPTY</i></div>
            </div>
          )
        }
        {
          orderCount ? (
            <div className="row mt-3">
              <div className='col-2 col-md-2'></div>
              <div className='col-6 col-md-5'><h4><b>TOTAL:</b></h4></div>
              <div className='col-4 col-md-5'>
                <h4><b>${Number(this.getTotal()).toFixed(2)}</b></h4>
              </div>
            </div>
          ) : ''
        }
        <div className='row mt-4 mb-5'>
          <div className='col-6 text-right'>
            <a href='/'>
              <button
                type="button"
                className="btn btn-outline-secondary"
              >
                SHOP MORE
              </button>
            </a>
          </div>
          <div className='col-6 text-left'>
            <button
              type="button"
              className={`btn btn-secondary ${checkout}`}
              onClick={() => !checkout && history.push('/checkout')}
            >
              CHECKOUT
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('mapStateToProps', state);
  return {
    order_items: state.cart.order.order_items
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (order) => dispatch(addToCart(order)),
    removeFromCart: (id) => dispatch(removeFromCart(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
