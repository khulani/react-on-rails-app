import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Cart from './cart/Cart';
import Checkout from './Checkout';
import Review from './Review';
import Confirm from './Confirm';
import { Provider } from 'react-redux';
import { StripeProvider, Elements } from 'react-stripe-elements';

export default class Order extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      store: ReactOnRails.getStore('store'),
      customer: {
        address: {
          full_name: '',
          street1: '',
          city: '',
          state: '',
          zip_code: ''
        },
        email: ''
      }
    };
  }

  updateCustomer = (customerUpdated) => {
    const { customer } = this.state;

    this.setState({ customer: { ...customer, ...customerUpdated } });
  }

  render() {
    const { store, customer } = this.state;
    const { cart_size } = this.props;

    return (
      <BrowserRouter basename='/order'>
        <Switch>
          <Route path='/cart'
            render={ props => (
              <Provider store={store}>
                <Cart {...props} />
              </Provider>
            ) }
          />
          <Route path='/checkout'
            render={ props => ( cart_size ? (
              <StripeProvider apiKey="pk_test_RRjTO4yWQeWRjOzVjvikDUus">
                <Elements>
                  <Checkout {...props}
                    customer={customer}
                    updateCustomer={this.updateCustomer}
                 />
                </Elements>
              </StripeProvider>
            ) : (
              <Redirect to="/cart" />
            )) }
          />
          <Route path='/review'
            render={ props => ( customer.payment ? (
              <Provider store={store}>
                <Review
                  {...props}
                  customer={customer}
                  updateCustomer={this.updateCustomer}
                />
              </Provider>
            ) : (
              <Redirect to="/checkout" />
            )) }
          />
          <Route path='/confirm'
            render={ props => ( customer.order_id ? (
              <Provider store={store}>
                <Confirm
                  {...props}
                  customer={customer}
                />
              </Provider>
            ) : (
              <Redirect to="/review" />
            )) }
          />
          <Redirect to="/cart" />
        </Switch>
      </BrowserRouter>
    );
  }
}
