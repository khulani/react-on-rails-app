import React from 'react';
import axios from 'axios';

export default class OrderItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      status: this.props.order_item.status || 'pending'
    };
  }

  redirectToItem = (id) => {
    const { order_item } = this.props;

    window.location.href = '/items/' + order_item.item.id;
  }

  updateStatus = (e) => {
    let status = e.nativeEvent.target.value;

    this.updateOrderItem(status);
  }

  cancelOrder = () => {
    this.updateOrderItem('cancelled');
  }

  updateOrderItem = (status) => {
    const { order_item, reduceTotal } = this.props;

    axios.put(`/order_items/${order_item.id}.json`, { order_item: { status } })
      .then(res => {
        this.setState({ status: status });
        if (status == 'cancelled')
          reduceTotal(order_item.price_paid * order_item.qty);
      })
      .catch((error) => {
        console.log('updateOrderItem error', error);
      });
  }

  getStatusOptions = () => {
    const { status } = this.state;

    if (status == 'pending') {
      return ['pending', 'accepted'];
    } else if (status == 'accepted') {
      return ['accepted', 'processing', 'shipped'];
    } else if (status == 'processing') {
      return ['processing', 'shipped'];
    } else {
      return ['processing', 'shipped', 'delivered']
    }
  }

  render() {
    const { order_item, edit } = this.props;
    const { status } = this.state;

    return (
      <div className='row cart-item-row'>
        { order_item.item.photo_url ? (
          <div
            className="col-3 col-md-2 cart-item-image"
            onClick={this.redirectToItem}
            style={
              {
                backgroundImage: `url(${order_item.item.photo_url})`
              }
            }
          />
        ) : (
          <div
            className="col-3 col-md-2 cart-item-image"
            onClick={this.redirectToItem}
            style={
              {
                backgroundColor: '#ececec'
              }
            }
          />
        ) }
        <div className='col-9 col-md-5'>
          <h3 className="card-title" onClick={this.redirectToItem}>
            {order_item.item.title}
          </h3>
          <p className="card-text">
            { order_item.selection ? Object.keys(order_item.selection).map(selection => (
              `${selection}: ${order_item.selection[selection]}`
            )).join(', ') : null }
          </p>
        </div>
        <div className='col-5 order-2 order-md-1 col-md-3 cart-item-price'>
          ${Number(order_item.price_paid || order_item.item.price).toFixed(2)}
        </div>
        <div className='col-4 order-3 order-md-2 col-md-2 cart-item-price'>
          {order_item.qty}
        </div>
        <div className="col-3 order-1 order-md-3 col-md-12"></div>

        <div className="col-3 col-md-2 order-4"></div>
        <div className='col-3 col-md-2 order-5 cart-item-price'>Status:</div>
        { edit && status != 'cancelled' ? (
          <div className='col-6 col-md-3 order-6 cart-item-qty'>
            <select
              className='form-control ml-1 item-qty-select'
              onChange={this.updateStatus}
              value={status}
            >
              { this.getStatusOptions().map(value => (
                <option key={value} value={value}>
                  {value}
                </option>
              )) }
            </select>
          </div>
        ) : (
          <div
            className='col-6 col-md-3 order-6 cart-item-price'
            style={{paddingLeft: '30px'}}
          >
            {status}
          </div>
        ) }

        <div
          className='col-12 col-md-4 order-7 cart-item-price text-center'
        >
          { edit && status != 'cancelled'? (
            <a
              data-toggle="modal"
              data-target={`#cancelModal${order_item.id}`}
              className='order-item-cancel'
            >
              Cancel/Refund
            </a>
          ) : null }
        </div>

        <div
          className="modal fade"
          id={`cancelModal${order_item.id}`}
          tabIndex="-1"
          role="dialog"
          aria-labelledby="cancelModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5
                  className="modal-title"
                  id="cancelModalLabel"
                >
                  {'Cancel ' + order_item.item.title}
                </h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                Are you sure?
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-danger"
                  onClick={this.cancelOrder}
                  data-dismiss="modal"
                >
                  Cancel
                </button>
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
