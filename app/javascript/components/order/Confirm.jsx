import React from 'react';
import { connect } from 'react-redux';
import { clearCart } from '../../store/actions/CartActions'
import OrderItem from './OrderItem'

class Confirm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      order_items: this.props.order_items
    };
  }

  componentDidMount() {
    this.props.clearCart();
  }

  getTotal = () => {
    const { order_items } = this.state;

    return (
      Object.values(order_items).reduce((total, order) => (
        total + Number(order.item.price) * Number(order.qty)
      ), 0)
    );
  }

  render() {
    const { order_items } = this.state;
    const { history, customer } = this.props;
    const orderCount = Object.keys(order_items).length;
    console.log('customer', customer);

    return (
      <div>
        <div className='row'>
          <div className='col-md-12 ml-4 mb-1'>
            <h1>Order Confirmation</h1>
          </div>
        </div>
        <div className='row'>
          <div className='col-12 mb-2' style={{fontSize: '1.5rem'}}>
            <i>Thank you. Your order has been placed.</i>
          </div>
          <div className='col-12'>
            Your order id is <b>{customer.order_id}</b>
          </div>
        </div>
        <div className='row mb-4'>
          <div className='col-md-5'>
            <div className='row cart-header checkout'>
              <div className='col-12' >Shipping Address</div>
            </div>
            <div className='row'>
              <div className='col-12'>{customer.address.full_name}</div>
              <div className='col-12'>{customer.address.street1}</div>
              <div className='col-12'>
                {customer.address.city}, {customer.address.state} {customer.address.zip_code}
              </div>
            </div>
          </div>
          <div className='col-md-1'></div>
          <div className='col-md-5'>
            <div className='row cart-header checkout'>
              <div className='col-12'>Credit Card</div>
            </div>
            <div className='row'>
              <div className='col-12'>{customer.payment.card.brand}</div>
              <div className='col-12'>xxxx-xxxx-xxxx-{customer.payment.card.last4}</div>
              <div className='col-12'>
                {customer.payment.card.exp_month} / {customer.payment.card.exp_year}
              </div>
            </div>
          </div>
        </div>
        <div className='row cart-header'>
          <div className='col-3 col-md-2'></div>
          <div className='col-9 col-md-5'>ITEM</div>
          <div
            className='col-3 d-block d-sm-block d-md-none text-right'
            style={{paddingRight: 0}}
          >
            &
          </div>
          <div className='col-5 col-md-3'>PRICE</div>
          <div className='col-4 col-md-2'>QTY</div>
        </div>
        {
          Object.keys(order_items).map(key => (
            <OrderItem key={key} order_item={order_items[key]} />
          ))
        }
        <div className="row mt-3 mb-5">
          <div className='col-1 col-md-2'></div>
          <div className='col-7 col-md-5'><h4><b>TOTAL:</b></h4></div>
          <div className='col-4 col-md-5'>
            <h4><b>${Number(this.getTotal()).toFixed(2)}</b></h4>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('mapStateToProps', state);
  return {
    order_items: state.cart.order.order_items
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    clearCart: () => dispatch(clearCart())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Confirm);
