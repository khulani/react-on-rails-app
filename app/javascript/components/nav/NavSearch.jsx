import React from 'react';
import axios from 'axios';
import NavSearchItem from './NavSearchItem';

export default class NavSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      query: '',
      items: [],
      loading: false
    };
  }

  search = (params) => {
    axios.get('/items/search.json', { params })
      .then(res => this.setState({ loading: false, items: res.data.items }))
      .catch(() => this.setState({ loading: false, items: [] }));
  }

  onChange = (e) => {
    const { value } = e.target;

    this.setState({ loading: true, query: value });
    if(this.timeout) clearTimeout(this.timeout);

    if (value.length > 0) {
      var params = { query: value };

      this.timeout = setTimeout(() => {
        this.search(params);
      }, 300);
    } else {
      this.setState({ items: [], loading: false });
    }
  }

  redirectToItem = (id) => {
    let url = '/items/' + id;
    if(window.location.pathname != url) {
      window.location.href = url;
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
  }

  render() {
    const { items, query, loading } = this.state;

    return (
      <form className="form-inline mr-sm-2 dropdown show" onSubmit={this.handleSubmit}>
        <input
          id='dropdownSearch'
          className="form-control"
          type="search"
          placeholder="Jump to item..."
          aria-label="Search"
          onChange={this.onChange}
          value={query}
        />
        { loading ? (
          <div className="dropdown-menu show"
            style={{width:'100%'}}
          >
            <a className="dropdown-item">
              Loading...
            </a>
          </div>
        ) : (
          query ? (
            <div className="dropdown-menu show"
              style={{width:'100%'}}
            >
              { items.length ? (
                items.map(item => (
                  <NavSearchItem key={item.id} item={item} />
                ))
              ) : (
                <a className="dropdown-item">
                  No results.
                </a>
              ) }
            </div>
          ) : null
        ) }
      </form>
    );
  }
}
