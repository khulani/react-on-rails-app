import React from 'react';
import axios from 'axios';

export default class NavSearch extends React.Component {
  redirectToItem = (id) => {
    let url = '/items/' + id;
    if(window.location.pathname != url) {
      window.location.href = url;
    }
  }

  render() {
    const { item } = this.props;

    return (
      <a className="dropdown-item" onClick={() => this.redirectToItem(item.id)}>
        <div>{item.title}</div>
        <p style={{fontSize: '0.9rem', marginBottom: '0px'}}>
          {item.user} - ${Number(item.price).toFixed(2)}
        </p>
      </a>
    );
  }
}
