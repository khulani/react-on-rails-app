import React from 'react';
import NavCart from './NavCart';
import NavUser from './NavUser';
import NavSearch from './NavSearch';
import { Provider } from 'react-redux';

const NavBar = (props) => {
  const { email } = props;
  const store = ReactOnRails.getStore('store');

  return (
    <nav className="navbar navbar-light navbar-expand-md bg-primary">
      <a className="navbar-brand" href="/">eCOMdemo</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMain" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarMain">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <a className="nav-link" href="/">HOME <span className="sr-only">(current)</span></a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/about">ABOUT</a>
          </li>
        </ul>
        <NavSearch />
        <ul className="navbar-nav">
          <li className="nav-item">
            <a className="nav-link" href="/items/new" style={{paddingTop: '11px'}}>
              SELL ITEM
            </a>
          </li>
          <Provider store={store}>
            <NavCart />
          </Provider>
          <NavUser email={email} />
        </ul>
      </div>
    </nav>
  );
}

export default NavBar;
