import React from 'react';
import { connect } from 'react-redux';
import { loadCart, removeFromCart } from '../../store/actions/CartActions';

class NavCart extends React.Component {
  componentDidMount() {
    this.props.loadCart();
  }

  redirectToItem = (id) => {
    let url = '/items/' + id;
    if(window.location.pathname != url) {
      window.location.href = url;
    }
  }

  render() {
    const { order_items, removeFromCart } = this.props;
    const orderCount = Object.keys(order_items).length;

    return (
      <li className="nav-item dropdown mb-2">
        <a
          className="nav-link dropdown-toggle"
          href="#" id="cartDropdown"
          role="button" data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          <div className='cart-icon' />
          {
            orderCount ? (
              <span className="badge badge-secondary cart-count">
                {orderCount}
              </span>
            ) : ''
          }
        </a>
        {
          orderCount ? (
            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="cartDropdown">
              { Object.keys(order_items).map(key => (
                <a key={key} className="dropdown-item">
                  <div className="card d-flex flex-row">
                    { order_items[key].item.photo_url ? (
                      <div
                        className="cart-item-image"
                        onClick={() => this.redirectToItem(key)}
                        style={
                          {
                            backgroundImage: `url(${order_items[key].item.photo_url})`
                          }
                        }
                      />
                    ) : (
                      <div
                        className="cart-item-image"
                        onClick={() => this.redirectToItem(key)}
                        style={
                          {
                            backgroundColor: '#ececec'
                          }
                        }
                      />
                    ) }
                    <div
                      className="card-body"
                      onClick={() => this.redirectToItem(key)}
                    >
                      <h3 className="card-title">{order_items[key].item.title}</h3>
                      <p className="card-text">
                        {`$${Number(order_items[key].item.price).toFixed(2)} (x ${order_items[key].qty})`}
                      </p>
                    </div>
                    <div
                      className="close"
                      onClick={() => removeFromCart(order_items[key].item.id)}
                    >
                      <span className="tag-close" aria-hidden="true">&times;</span>
                    </div>
                  </div>
                </a>
              )) }
              <div className="dropdown-divider"></div>
              <a className="dropdown-item" href='/order/cart'>VIEW CART...</a>
            </div>
          ) : (
            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="cartDropdown">
              <a className="dropdown-item">Empty</a>
            </div>
          )
        }
      </li>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('NavCart store', state);
  return {
    order_items: state.cart.order.order_items
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadCart: () => dispatch(loadCart()),
    removeFromCart: (id) => dispatch(removeFromCart(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavCart);
