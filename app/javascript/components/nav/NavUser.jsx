import React from 'react';

export default class NavUser extends React.Component {
  render() {
    const { email }= this.props;

    return (
      <li className="nav-item dropdown mb-2">
        <a
          className="nav-link dropdown-toggle"
          href="#" id="userDropdown"
          role="button" data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          <div className='user-icon' />
        </a>
        { email ? (
          <div className="dropdown-menu dropdown-menu-right text-center" aria-labelledby="userDropdown">
            <a href='/user/profile' className="dropdown-item">Profile</a>
            <a href='/user/items' className="dropdown-item">Items</a>
            <a href='/user/orders' className="dropdown-item">Orders</a>
            <a href='/user/customers' className="dropdown-item">Customers</a>
            <div className="dropdown-divider"></div>
            <a href='/logout' className="dropdown-item">Log out</a>
          </div>
        ) : (
          <div className="dropdown-menu dropdown-menu-right text-center" aria-labelledby="userDropdown">
            <a href='/login' className="dropdown-item">Log in</a>
            <div className="dropdown-divider"></div>
            <a href='/signup' className="dropdown-item">Sign up</a>
          </div>
        ) }
      </li>
    );
  }
}
