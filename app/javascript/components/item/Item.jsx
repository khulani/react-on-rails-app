import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import axios from 'axios';
import ItemShow from './ItemShow';
import ItemForm from './ItemForm';
import { Provider } from 'react-redux';

export default class Item extends React.Component {
  constructor(props) {
    super(props);

    var item = Object.assign({
      title: null,
      price: null,
      sale_price: null,
      description: null,
      categories: [],
      distinctions: [],
      photos: []
    }, this.props.item);

    this.state = {
      view: this.props.view,
      item: item,
      notice: this.props.notice || {},
      status: 'success',
      errors: {},
      editable: this.props.editable,
      saved_item: JSON.stringify(item),
      store: ReactOnRails.getStore('store')
    };
  }

  updateItem = (name, value) => {
    const { item } = this.state;
    item[name] = value;
    this.setState({
      item: item
    });
  }

  trackItem = () => {
    const { item } = this.state;
    this.setState({
      saved_item: JSON.stringify(item)
    });
  }

  restoreItem = () => {
    const { saved_item } = this.state;
    if (saved_item) {
      this.setState({
        item: JSON.parse(saved_item), errors: {}, notice: {}
      });
    }
  }

  validateItem = () => {
    const { item, errors } = this.state;

    if (!item.title) {
      errors.title = 'Title must exist.';
    } else {
      errors.title && delete errors.title;
    }

    if (!item.price) {
      errors.price = 'Price must exist.';
    } else if (isNaN(item.price)) {
      errors.price = 'Price must be a number.';
    } else {
      errors.price && delete errors.price;
    }

    if (item.sale_price && isNaN(item.sale_price)){
      errors.sale_price = 'Sale price must be a number';
    } else {
      errors.sale_price && delete errors.sale_price;
    }

    if (item.categories == undefined || item.categories.length == 0) {
      errors.categories = 'Categoties cannot be empty.';
    } else {
      errors.categories && delete errors.categories;
    }

    for (var i = 0; i < item.distinctions.length; i++) {
      if (item[item.distinctions[i]] == undefined || item[item.distinctions[i]].length == 0) {
        errors[item.distinctions[i]] = item.distinctions[i] + ' cannot be empty.';
      } else {
        errors[item.distinctions[i]] && delete errors[item.distinctions[i]];
      }
    }

    this.setState({errors: errors});
    console.log(errors);
    return (Object.keys(errors).length == 0);
  }

  getError = (field) => {
    const { errors } = this.state;
    return (errors[field]);
  }

  clearError = (field) => {
    const { errors } = this.state;
    errors[field] && delete errors[field];
    this.setState({errors: errors});
  }

  saveItem = (history) => {
    const { item } = this.state;
    if (item.id) {
      axios.put(`/items/${item.id}.json`, { item: item })
        .then(res => {
          this.setState({
            item: res.data,
            notice: {message: 'Item updated.', type: 'success'}
          });
          window.scrollTo(0, 0);
          history.push(`/${item.id}`);
        })
        .catch((error) => {
          this.setState({
            notice: {message: 'Unable to update item.', type: 'danger'}
          });
          console.log(error);
        });
    } else {
      axios.post('/items.json', { item: item })
        .then(res => {
          this.setState({
            item: res.data,
            notice: {message: 'Item created.', type: 'success'},
            editable: true
          });
          window.scrollTo(0, 0);
          history.push(`/${res.data.id}`);
        })
        .catch(() => this.setState({
          notice: {message: 'Unable to create item.', type: 'danger'}
        }));
    }
  }

  deleteItem = () => {
    const { item } = this.state;
    axios.delete('/items/' + item.id + '.json', { item: item })
      .then(res => {
        this.setState({
          notice: {message: 'Item deleted.', type: 'success'},
          editable: false
        });
        window.scrollTo(0, 0);
      })
      .catch(() => this.setState({
        notice: {message: 'Unable to delete item.', type: 'danger'}
      }));
  }

  render() {
    const { view, item, notice, editable, store } = this.state;
    return (
      <BrowserRouter basename='/items'>
        <Switch>
          <Route path="/:id/edit"
            render={(props)=>(
              <ItemForm
                {...props}
                item={item}
                updateItem={this.updateItem}
                restoreItem={this.restoreItem}
                validateItem={this.validateItem}
                saveItem={this.saveItem}
                getError={this.getError}
                clearError={this.clearError}
              />
            )}
          />
          <Route path="/new"
            render={(props)=>(
              <ItemForm
                {...props}
                item={item}
                updateItem={this.updateItem}
                restoreItem={this.restoreItem}
                validateItem={this.validateItem}
                saveItem={this.saveItem}
                getError={this.getError}
                clearError={this.clearError}
              />
            )}
          />
          <Route path="/:id"
            render={(props)=>(
              <Provider store={store}>
                <ItemShow
                  {...props}
                  item={item}
                  notice={notice}
                  editable={editable}
                  trackItem={this.trackItem}
                  deleteItem={this.deleteItem}
                />
              </Provider>
            )}
          />
        </Switch>
      </BrowserRouter>
    );
  }
}
