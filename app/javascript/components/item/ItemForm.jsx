import React from 'react';
import Tagger from './form/tagger/Tagger';
import TitleInput from './form/TitleInput';
import PriceInput from './form/PriceInput';
import Description from './form/Description';
import Photos from './form/Photos';

export default class ItemForm extends React.Component {
  updateItem = (name, value) => {
    this.props.clearError(name);
    this.props.updateItem(name, value);
  }

  onCancel = (e) => {
    this.props.restoreItem();
    this.props.history.push(`/${this.props.item.id}`);
  };

  saveItem = (e) => {
    if (this.props.validateItem()) {
      const { photos } = this.props.item;
      var photo_ids = photos.map(photo => photo.id);
      this.props.updateItem('photo_ids', photo_ids);

      this.props.saveItem(this.props.history);
    }
  };

  updatePhoto = (index, photo) => {
    var photos = this.props.item.photos;

    if (!isNaN(index) && photo) {
      photos[index] = photo;
    } else if (!isNaN(index)) {
      photos = photos.slice(0, index).concat(photos.slice(index + 1, photos.length));
    } else if (photo) {
      photos.push(photo);
    }

    this.props.updateItem('photos', photos);
  }

  renderError = (field) => {
    var message = this.props.getError(field);
    if (message) {
      return (
        <div className="alert alert-danger" role="alert">
          {message}
        </div>
      );
    }
  }

  render() {
    const { item } = this.props;

    return (
      <div className="row">
        <div className="col-md-2"></div>
        <div className="col-md-8">
          <form>
            <TitleInput
              title={item.title}
              renderError={this.renderError}
              updateItem={this.updateItem}
            />
            <PriceInput
              price={item.price}
              sale_price={item.sale_price}
              renderError={this.renderError}
              updateItem={this.updateItem}
            />
            <Description
              description={item.description}
              updateItem={this.updateItem}
            />
            <Tagger
              key='categories'
              tag='categories'
              values={item['categories']}
              updateParentState={this.updateItem}
              getError={this.props.getError}
              clearError={this.props.clearError}
            />
            <Tagger
              key='distinctions'
              tag='distinctions'
              values={item['distinctions']}
              updateParentState={this.updateItem}
              getError={this.props.getError}
              clearError={this.props.clearError}
              optional={true}
            />
            {item.distinctions.map((distinction) => (
              <Tagger
                key={distinction}
                tag={distinction}
                values={item[distinction] || []}
                url='choices'
                updateParentState={this.updateItem}
                getError={this.props.getError}
                clearError={this.props.clearError}
              />
            ), this)}
            <Photos
              photos={item.photos}
              updatePhoto={this.updatePhoto}
            />
            <div className="form-group row">
              <div className="col-sm-3"></div>
              <div className="col-sm-9 text-center d-block d-sm-none">
                <button type="button" className="btn btn-primary" onClick={this.saveItem}>Save</button>
                { item.id ? (
                  <button type="button" className="btn btn-link" onClick={this.onCancel}>Cancel</button>
                ) : (
                  <a type="button" href='/' className="btn btn-link">Return</a>
                )}
              </div>
              <div className="col-sm-9 text-center d-none d-sm-block">
                <button type="button" className="btn btn-primary" onClick={this.saveItem}>Save</button>
                { item.id ? (
                  <button type="button" className="btn btn-link" onClick={this.onCancel}>Cancel</button>
                ) : (
                  <a type="button" href='/' className="btn btn-link">Return</a>
                )}
              </div>
            </div>
          </form>
        </div>
        <div className="col-md-2"></div>
      </div>
    );
  }
}
