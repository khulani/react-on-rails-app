import React from 'react';
import { connect } from 'react-redux';
import { addToCart } from '../../store/actions/CartActions'

class ItemShow extends React.Component {
  constructor(props) {
    super(props);
    console.log('order_items', this.props.order_items);
    this.state = {
      item: this.props.item,
      mainPhoto: 0,
      selection: {},
      qty: 1
    };
  }

  componentDidUpdate(prevProps) {
    const { item, order_items } = this.props;

    if (order_items !== prevProps.order_items) {
      if (order_items[item.id]) {
        this.setState({
          selection: order_items[item.id].selection || {},
          qty: order_items[item.id].qty
        });
      } else {
        this.setState({
          selection: {},
          qty: 1
        });
      }
    }
  }

  editItem = () => {
    const { item } = this.state;
    this.props.trackItem();
    this.props.history.push(`/${item.id}/edit`);
  }

  showPhoto = (index) => {
    this.setState({mainPhoto: index});
  }

  updateSelection = (distinction, choice) => {
    const { selection } = this.state;

    this.setState({ selection: { ...selection, [distinction]: choice } });
  };

  addToCart = () => {
    const { item, selection, qty } = this.state;
    let orderItem = {
      id: item.id,
      title: item.title,
      price: item.sale_price || item.price,
      photo_url: item.photos[0] ? item.photos[0].photo_url : null
    };
    let order = { item: orderItem, qty, selection };
    this.props.addToCart(order);
  }

  updateQty = (e) => {
    let qty = e.nativeEvent.target.value;
    console.log('qty:', qty);
    this.setState({qty});
  }

  render() {
    const { item, mainPhoto, selection, qty } = this.state;
    const qtyChoices = [...Array(10).keys()];
    const order = this.props.order_items[item.id];

    return (
      <div className='row' style={{marginTop: '20px'}}>
        { this.props.notice.message ? (
          <div className="col-12 text-center">
            <div className={"alert alert-" + this.props.notice.type} role="alert">
              {this.props.notice.message}
            </div>
          </div>
        ) : '' }
        <div className='col-12 d-block d-sm-block d-md-none' style={{marginBottom: '10px'}}>
          <div className='item-categories'>
            {item.categories.join(', ')}
          </div>
          <div className='item-show-title'>{item.title}</div>
          <div className='item-seller'>{'by ' + item.user}</div>
        </div>
        <div className='col-md-2 col-lg-2 col-xl-2 d-none d-sm-none d-md-block'>
          <div className='row'>
            {item.photos.map((photo, index) => (
              <div key={index} className='col-12 text-right'>
                <div
                  onClick={() => this.showPhoto(index)}
                  className="item-image-choice"
                  style={
                    {
                      backgroundImage: 'url('+ (photo.photo_url) +')'
                    }
                  }
                />
              </div>
            ))}
          </div>
        </div>
        <div className='col-12 d-block d-sm-block d-md-none' style={{marginTop: '10px'}}>
          {item.photos.map((photo, index) => (
            <div
              key={index}
              onClick={() => this.showPhoto(index)}
              className="item-image-choice"
              style={
                {
                  backgroundImage: 'url('+ (photo.photo_url) +')'
                }
              }
            />
          ))}
        </div>
        <div className='col-md-4 col-lg-4 col-xl-4 d-none d-sm-none d-md-block'>
          { item.photos[0] ? (
            <div className='item-image-expand'>
              <img src={item.photos[mainPhoto].photo_url} className='item-image-main' />
            </div>
          ) : (
            <div className="item-image-container" style={{backgroundColor: '#ececec'}} />
          ) }
        </div>
        <div className='col-md-4 col-lg-4 col-xl-4 d-block d-sm-block d-md-none'>
          { item.photos[0] ? (
              <img src={item.photos[mainPhoto].photo_url} className='item-image-main' />
          ) : '' }
        </div>
        <div className='col-md-6 col-lg-6'>
          <div className="row d-none d-sm-none d-md-block">
            <div className='col-12 item-categories'>
              {item.categories.join(', ')}
            </div>
            <div className="col-12 item-show-title">{item.title}</div>
            <div className="col-12 item-seller">{'by ' + item.user}</div>
          </div>
          <div className='row' style={{marginTop: '10px', marginBottom: '5px'}}>
            { item.sale_price ? (
              <div className="col-12">
                <span className='item-price'>{'$' + Number(item.sale_price).toFixed(2)}</span>
                <span className='item-original-price'>{'$' + Number(item.price).toFixed(2)}</span>
              </div>
            ) : (
              <div className="col-12">
                <span className='item-price'>{'$' + Number(item.price).toFixed(2)}</span>
              </div>
            ) }
            <div className="col-12 item-description">{item.description}</div>
          </div>

          {item.distinctions.map(distinction => (
            <div key={distinction} className="row" style={{marginBottom: '10px'}}>
              <div className="col-12 item-distinction">{distinction + ':'}</div>
              <div className="col-12">
                {item[distinction].map((choice, index) => (
                  <label
                    key={choice}
                    htmlFor={distinction + '_' + index}
                    className={ selection[distinction] == choice ? (
                      'btn btn-sm btn-outline-secondary tag-label active'
                    ) : (
                      'btn btn-sm btn-outline-secondary tag-label'
                    )}
                    onClick={() => this.updateSelection(distinction, choice)}
                  >
                    {choice}
                    <input
                      type="radio"
                      name={distinction}
                      value={choice}
                      id={distinction + '_' + index}
                      className="item-radio-hidden"
                    />
                  </label>
                ))}
              </div>
            </div>
          ))}
          <div className='row mt-4 mb-5 d-block d-sm-block d-md-none'>
            { this.props.editable ? (
              <div className="col-12 text-center">
                <button
                  className="btn btn-primary mr-1"
                  onClick={this.editItem}
                >
                  Edit
                </button>
                <button
                  className="btn btn-danger"
                  data-toggle="modal"
                  data-target="#deleteModal"
                >
                  Delete
                </button>
              </div>
            ) : (
              <div className="col-12">
                <form className="form-inline">
                  <div className="form-group mb-0 mr-3">
                    <label className='item-qty-label'>QTY:</label>
                    <select
                      className='form-control ml-1 item-qty-select'
                      onChange={this.updateQty}
                      value={qty}
                    >
                      { qtyChoices.map((_, i) => (
                        <option key={i} value={i + 1}>
                          {i + 1}
                        </option>
                      )) }
                    </select>
                  </div>
                  <button
                    type="button"
                    className="btn btn-success"
                    onClick={this.addToCart}
                  >
                    { order ? 'Update Cart' : 'Add to Cart' }
                  </button>
                </form>
              </div>
            ) }
          </div>
          <div className='row mt-4 mb-5 d-none d-sm-none d-md-block'>
            { this.props.editable ? (
              <div className="col-12">
                <button
                  className="btn btn-primary mr-1"
                  onClick={this.editItem}
                >
                  Edit
                </button>
                <button
                  className="btn btn-danger"
                  data-toggle="modal"
                  data-target="#deleteModal"
                >
                  Delete
                </button>
              </div>
            ) : (
              <div className="col-12">
                <form className="form-inline">
                  <div className="form-group mb-0 mr-3">
                    <label className='item-qty-label'>QTY:</label>
                    <select
                      className='form-control ml-1 item-qty-select'
                      onChange={this.updateQty}
                      value={qty}
                    >
                      { qtyChoices.map((_, i) => (
                        <option key={i} value={i + 1}>
                          {i + 1}
                        </option>
                      )) }
                    </select>
                  </div>

                  <button
                    type="button"
                    className="btn btn-success"
                    onClick={this.addToCart}
                  >
                    { order ? 'Update Cart' : 'Add to Cart' }
                  </button>
                </form>
              </div>
            ) }
          </div>
          { this.props.editable ? (
            <div className="modal fade" id="deleteModal" tabIndex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="deleteModalLabel">{'Delete ' + item.title}</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    Are you sure?
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-danger"
                      onClick={() => this.props.deleteItem()}
                      data-dismiss="modal"
                    >
                      Delete
                    </button>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  </div>
                </div>
              </div>
            </div>
          ) : '' }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log('mapStateToProps', state);
  return {
    order_items: state.cart.order.order_items
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (order) => dispatch(addToCart(order))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemShow)
