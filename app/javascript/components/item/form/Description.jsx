import React from 'react';

export default class Description extends React.Component {
  render() {
    return (
      <div className="form-group row">
        <label
          type='text'
          className='col-sm-3 col-form-label d-block d-sm-none'
        >
          DESCRIPTION:
        </label>
        <label
          type='text'
          className='col-sm-3 col-form-label text-right d-none d-sm-block'
        >
          DESCRIPTION:
        </label>
        <div className="col-sm-9">
          <textarea
            defaultValue={this.props.description}
            className="form-control"
            rows="4"
            cols="50"
            placeholder="Product Description"
            onChange={(e) => this.props.updateItem('description', e.target.value)}
          ></textarea>
        </div>
      </div>
    );
  }
}
