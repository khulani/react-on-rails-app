import React from 'react';
import axios from 'axios';

export default class Photo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      photo: this.props.photo || {}
    };
  }

  uploadPhoto = e => {
    const fd = new FormData();
    const { photo } = this.state;
    const file =  e.target.files[0];

    if (file.size > 1000000) {
      this.setState({error: 'File size limit is 1MB'})
    } else {
      this.setState({error: false})
      fd.append('photo', file, new Date().getTime().toString() + '_' + file.name);

      axios.post('/photos/', fd)
        .then(res => this.props.updatePhoto('add', res.data))
        .catch(() => this.setState({ errors: 'Unable to add photo' }));
    }
  }

  deletePhoto = e => {
    const { photo } = this.state;

    axios.delete('/photos/' + photo.id)
      .then(() => this.props.updatePhoto(this.props.index, false))
      .catch(() => this.setState({ errors: 'Unable to delete photo' }));
  }

  render() {
    const { photo, error } = this.state;
    return (
      <div style={{float: 'left'}}>
        <input
          type="file"
          style={{display: 'none'}}
          className="form-control"
          onChange={this.uploadPhoto}
          ref={inputPhoto => this.inputPhoto = inputPhoto}
        />
        { isNaN(this.props.index) ? (
          <div className='item-image-add align-middle text-center' onClick={() => this.inputPhoto.click()}>
            <span>{'+'}</span>
          </div>
        ) : (
          <div
            className='item-image-form'
            style={
              {
                backgroundImage: 'url('+ (photo.photo_url) +')'
              }
            }
          >
            <div className="close" aria-label="Close" onClick={() => this.deletePhoto()}>
              <span aria-hidden="true">&times;</span>
            </div>
          </div>
        )}
        { error ? (
          <div className="alert alert-danger" style={{marginTop: '5px'}} role="alert">
            {error}
          </div>
        ) : '' }
      </div>
    );
  }
}
