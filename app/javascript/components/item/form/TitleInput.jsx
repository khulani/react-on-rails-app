import React from 'react';

export default class TitleInput extends React.Component {
  render() {
    return (
      <div className="form-group row">
        <label
          type='text'
          htmlFor='item_title'
          className='col-sm-3 col-form-label d-block d-sm-none'
        >
          TITLE:
        </label>
        <label
          type='text'
          htmlFor='item_title'
          className='col-sm-3 col-form-label text-right d-none d-sm-block'
        >
          TITLE:
        </label>
        <div className="col-sm-9">
          <input
            type="text"
            onChange={(e) => this.props.updateItem('title', e.target.value)}
            defaultValue={this.props.title}
            className="form-control"
            placeholder="Product Name"
            autoFocus
          />
          {this.props.renderError('title')}
        </div>
      </div>
    );
  }
}
