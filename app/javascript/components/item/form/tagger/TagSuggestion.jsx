import React from 'react';

export default ({ result, handleClick }) => (
  <div className='tag-suggestion' onClick={() => handleClick(result.name)}>{result.name}</div>
);
