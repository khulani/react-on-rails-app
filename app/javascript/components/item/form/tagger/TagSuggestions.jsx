import React from 'react';
import TagSuggestion from './TagSuggestion';

export default ({ results, loading, handleClick }) => (
  <div
    className="col-sm-12"
    style={{
      position: 'relative',
      zIndex: 1,
      width: '100%',
      padding: '0px'
    }}
  >
    {loading ? (
      <div className="tag-list">
        <div className="tag-suggestion">Loading...</div>
      </div>
    ) : (
      <div className="tag-list">
        {results.map(result => <TagSuggestion key={result.id} result={result} handleClick={handleClick}  />)}
      </div>
    )}
  </div>
);
