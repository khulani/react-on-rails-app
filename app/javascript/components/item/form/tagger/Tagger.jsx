import React from 'react';
import axios from 'axios';
import TagSuggestions from './TagSuggestions';

export default class Tagger extends React.Component {

  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.keyPress = this.keyPress.bind(this);
    this.renderError = this.renderError.bind(this);
    this.search = this.search.bind(this);
    this.timeout = 0;
    this.state = {
      loading: false,
      results: [],
      name: '',
      // tag: this.props.tag,
      // values: this.props.values || [],
      url: (this.props.url || this.props.tag),
      error: '',
      // optional: this.props.optional
    };
  }

  handleClick = (value) => {
    const { tag, values } = this.props;
    value = value.toUpperCase();
    if (values.includes(value)) {
      this.setState({name: '', results: [], error: `\`${value}\` already exists`});
    } else if (value != '') {
      var updated_values = [...values, value];
      this.setState({name: '', results: []});
      console.log(updated_values);
      this.props.updateParentState(tag, updated_values);
    }
  }

  handleDelete(i) {
    const { tag, values, optional } = this.props;
    var updated_values = values.filter((values, index) => index !== i);
    this.props.updateParentState(tag, updated_values);
  }

  keyPress(e) {
    if(e.keyCode == 13){
      e.preventDefault();
      this.handleClick(e.target.value);
    }
  }

  buttonClick = () => {
    const { name } = this.state;
    this.handleClick(name);
  }

  search = (params) => {
    axios.get('/' + this.state.url + '.json', { params: params })
      .then(res => this.setState({ loading: false, results: res.data }))
      .catch(() => this.setState({ loading: false, results: [] }));
  }

  onChange = (e) => {
    const { value } = e.target;
    const { url } = this.state;
    const { tag } = this.props;
    this.props.clearError(tag);
    this.setState({loading: true, name: value, error: ''});
    if(this.timeout) clearTimeout(this.timeout);

    if (value.length > 0) {
      var params = { query: value };
      if (url != tag) {
        params['distinction'] = tag;
      }

      this.timeout = setTimeout(() => {
        this.search(params);
      }, 300);
    } else {
      this.setState({ loading: false, results: [] });
    }
  }

  renderError() {
    const { error } = this.state;
    const { tag } = this.props;
    var message = error || this.props.getError(tag);
    if (message) {
      return (
        <div className="alert alert-danger" role="alert">
          {message}
        </div>
      )
    }
  }

  render() {
    const { loading, results } = this.state;
    const { tag, values } = this.props;
    return (
      <div className="form-inline tag-form mb-3 row">
        <label
          type='text'
          className='col-sm-3 col-form-label d-block d-sm-none'
        >
          {`${tag.toUpperCase()}:`}
        </label>
        <label
          type='text'
          className='col-sm-3 col-form-label text-right d-none d-sm-block'
        >
          {`${tag.toUpperCase()}:`}
        </label>
        <div className="col-sm-9">
          <div style={{float:'left'}}>
            {values.map((value, index) => (
              <div key={index} className='btn btn-outline-secondary tag-label'>
                <span className='tag-value'>{value}</span>
                <div className="close" aria-label="Close" onClick={() => this.handleDelete(index)}>
                  <span className="tag-close" aria-hidden="true">&times;</span>
                </div>
              </div>
            ))}
          </div>
          <div style={{float:'left'}}>
            <input
              type='text'
              size='7'
              className="form-control"
              onChange={this.onChange}
              value={this.state.name}
              onKeyDown={this.keyPress}
              placeholder={this.props.id}
              autoComplete="off"
            />
            <button type="button" className="btn btn-tag" onClick={this.buttonClick}>ADD</button>
            { this.renderError() }
            {results.length > 0 || loading ? (
              <TagSuggestions results={results} loading={loading} handleClick={this.handleClick} />
            ) : null}
          </div>


        </div>
      </div>
    );
  }
}
