import React from 'react';

export default class PriceInput extends React.Component {
  render() {
    return (
      <div className="form-group row">
        <label
          type='text'
          className='col-sm-3 col-form-label d-block d-sm-none'
        >
          PRICE:
        </label>
        <label
          type='text'
          className='col-sm-3 col-form-label text-right d-none d-sm-block'
        >
          PRICE:
        </label>
        <div className="col-sm-3">
          <input
            type="text"
            defaultValue={this.props.price}
            className="form-control"
            onChange={(e) => this.props.updateItem('price', e.target.value)}
            placeholder="9.99"
          />
          {this.props.renderError('price')}
        </div>
        <label
          type='text'
          className='col-sm-3 col-form-label d-block d-sm-none'
        >
          {'SALE PRICE: '}
          <span style={{color: '#555', fontWeight: 'normal', fontSize: '0.8rem'}}>(OPTIONAL)</span>
        </label>
        <label
          type='text'
          className='col-sm-3 col-form-label text-right d-none d-sm-block'
        >
          {'SALE PRICE: '}
          <span style={{color: '#555', fontWeight: 'normal', fontSize: '0.8rem'}}>(OPTIONAL)</span>
        </label>
        <div className="col-sm-3">
          <input
            type="text"
            defaultValue={this.props.sale_price}
            className="form-control"
            placeholder="5.99"
            onChange={(e) => this.props.updateItem('sale_price', e.target.value)}
          />
          {this.props.renderError('sale_price')}
        </div>
      </div>
    );
  }
}
