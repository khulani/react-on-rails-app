import React from 'react';
import Photo from './Photo';

export default class Photos extends React.Component {

  render() {
    const { photos } = this.props;
    return (
      <div className="form-group row">
        <label
          type='text'
          htmlFor='item_photo'
          className='col-sm-3 col-form-label d-block d-sm-none'
        >
          PHOTOS:
        </label>
        <label
          type='text'
          htmlFor='item_photo'
          className='col-sm-3 col-form-label text-right d-none d-sm-block'
          style={{lineHeight: '130px'}}
        >
          PHOTOS:
        </label>
        <div className="col-sm-9">
          {photos.map((photo, index) => (
            <Photo
              key={photo.id}
              photo={photo}
              index={index}
              updatePhoto={this.props.updatePhoto}
            />
          ), this)}
          <Photo
            key='new'
            photo={{}}
            updatePhoto={this.props.updatePhoto}
          />
        </div>
      </div>
    );
  }
}
