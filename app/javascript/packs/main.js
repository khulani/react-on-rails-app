import ReactOnRails from 'react-on-rails';
import store from '../store/Store';
import Item from '../components/item/Item';
import Items from '../components/items/Items';
import NavBar from '../components/nav/NavBar';
import Order from '../components/order/Order';
import User from '../components/user/User';

ReactOnRails.registerStore({
  store
});

ReactOnRails.register({
  NavBar,
  Item,
  Items,
  Order,
  User
});
