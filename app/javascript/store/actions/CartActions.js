import axios from 'axios';

export const addToCart = (order) => {
  return (dispatch, getState) => {
    var order_params = {
      item_id: order.item.id,
      qty: order.qty,
      selection: order.selection
    }
    axios.post('/order_items.json', { order_item: order_params })
      .then(res => {
        dispatch({ type: 'ADD_ORDER', order: order });
      })
      .catch(error => console.log('addToCart error:', error));
  };
}

export const removeFromCart = (id) => {
  return (dispatch, getState) => {
    axios.delete('/order_items/' + id + '.json')
      .then(res => {
        dispatch({ type: 'REMOVE_ORDER', id: id });
      })
      .catch(error => console.log('removeFromCart error:', error));
  }
}

export const loadCart = () => {
  return (dispatch, getState) => {
    // async
    axios.get('/orders/cart.json')
      .then(res => {
        dispatch({ type: 'LOAD_CART', cart: res.data.cart });
      })
      .catch(error => console.log('loadCart error:', error));
  }
}

export const clearCart = () => {
  return (dispatch, getState) => {
    dispatch({ type: 'CLEAR_CART' });
  }
}
