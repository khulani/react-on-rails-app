import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers/RootReducer';
import thunk from 'redux-thunk';

const store = () => { return createStore(rootReducer, applyMiddleware(thunk)) };

export default store;
//
// cartStore.subscribe(() => {
//   console.log('store changed', cartStore.getState());
// });
//
// store.dispatch({type: 'INC', payload: 1});
