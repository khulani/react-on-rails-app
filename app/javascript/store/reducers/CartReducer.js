const initState = {
  order: { order_items: {} }
};

const cartReducer = (state = initState, action) => {
  let order_items;

  switch (action.type) {
    case 'ADD_ORDER':
      order_items = {
        ...(state.order.order_items),
        [action.order.item.id]: action.order
      };
      return {
        ...state,
        order: {
          ...(state.order),
          order_items: order_items
        }
      };
    case 'REMOVE_ORDER':
      order_items = {
        ...(state.order.order_items),
      };
      delete order_items[action.id];
      return {
        ...state,
        order: {
          ...(state.order),
          order_items: order_items
        }
      };
    case 'LOAD_CART':
      if (!action.cart.order_items) action.cart.order_items = {}
      return { ...state, order: action.cart};
    case 'CLEAR_CART':
      return { ...state, order: { order_items: {} } };
  }

  return state;
}

export default cartReducer;
