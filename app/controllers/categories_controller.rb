class CategoriesController < ApplicationController
  skip_before_action :require_login, only: [:index, :show]

  def index
    @categories = Category.where('name LIKE ?', "%#{params[:query]&.upcase}%")
  end
end
