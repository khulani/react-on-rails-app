class ChoicesController < ApplicationController
  skip_before_action :require_login, only: [:index, :show]

  def index
    @choices = Choice.where(
      "name LIKE ? AND distinction = ?",
      "%#{params[:query]&.upcase}%", params[:distinction]
    )
  end
end
