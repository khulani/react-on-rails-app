class PhotosController < ApplicationController
  skip_forgery_protection
  skip_before_action :require_login, only: [:index, :show]

  def create
    @photo = Photo.create
    @photo.photo = params[:photo]
    if @photo.save
      render 'show.json'
    else
      render json: @photo.errors.full_messages, status: :unprocessable_entity
    end
  end

  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy if @photo.item.blank? || @photo.item.user == current_user

    if @photo.destroyed?
      render json: { message: 'success' }, status: :ok
    else
      render json: { errors: @photo.errors.full_messages }, status: :unprocessable_entity
    end
  end
end
