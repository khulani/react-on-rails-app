class ApplicationController < ActionController::Base
  protect_from_forgery unless: -> { request.format.json? }
  helper_method :current_user
  before_action :require_login
  before_action :ensure_anonymous

  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    else
      @current_user = nil
    end
  end

  private

  def require_login
    unless current_user
      flash[:error] = "Please log in."
      redirect_to new_session_url
    end
  end

  def ensure_anonymous
    puts "cookies: #{cookies.map{|k, v| [k, v]}}"
    unless cookies[:anonymous]
      cookies[:anonymous] = SecureRandom.uuid
    end

    @anonymous = cookies[:anonymous]
  end
end
