class ItemsController < ApplicationController
  before_action :require_login, only: [:new, :create, :edit, :update, :destroy]

  def index
    limit = params[:limit] || 12
    offset = (params[:page].to_i || 0) * limit

    if params[:user_id]
      @items = User.find_by(id: params[:user_id]).items
        .includes(:categories, :distinctions, :choices, :photos)
    else
      @items = Item.includes(:categories, :distinctions, :choices, :photos).all
    end

    if params[:query]
      @items = @items.where(
        'UPPER(title) LIKE :query OR categories.name LIKE :query',
        query: "%#{params[:query].upcase}%"
      ).references(:categories)
    end

    @categories = category_counts

    if params[:category]
      @items = @items.where(categories: { name: params[:category] })
    end

    @items = @items.limit(limit).offset(offset)
    @pages = (@categories[params[:category] || 'ALL'] + (limit - 1)) / limit
  end

  def new
  end

  # item: {
  #   title: 'Title',
  #   price: '9.99',
  #   sale_price: '5.99',
  #   categories: ['education', 'games'],
  #   description: 'Product Description...',
  #   distinctions: ['size', 'color'],
  #   size: ['Small', 'Medium', 'Large'],
  #   color: ['Red', 'Blue', 'Yellow']
  # }
  def create
    @item = Item.new item_params.merge(user: current_user)

    respond_to do |format|
      if @item.save
        @item.update_categories params[:item][:categories]
        @item.update_distinctions params[:item][:distinctions]
        @item.photo_ids = params[:item][:photo_ids]
        @item.update_choices item_choices

        format.html { redirect_to @item, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @item }
      else
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @item = Item.find_by(id: params[:id])
    return redirect_to root_path, notice: { message: 'Item not found.', type: 'warning'} unless @item
    session[:source] = item_path(@item)
    @notice = notice
  end

  def edit
    @item = Item.find_by(id: params[:id], user_id: current_user.id)
    if @item
      render :edit
    else
      redirect_to item_path(params[:id]), notice: { message: 'Item not editable.', type: 'warning'}
    end
  end

  def update
    @item = Item.find_by(id: params[:id], user_id: current_user.id)
    return redirect_to item_path(params[:id]) unless @item

    if @item.update item_params
      @item.update_categories params[:item][:categories]
      @item.update_distinctions params[:item][:distinctions]
      @item.photo_ids = params[:item][:photo_ids]
      @item.update_choices item_choices

      render 'show.json'
    else
      render json: @items.errors, status: :unprocessable_entity
    end
  end

  def destroy
    item = Item.find_by(id: params[:id], user_id: current_user.id)
    if item&.destroy
      render json: { message: 'Item deleted.', type: 'success' }, status: :ok
    else
      render json: { message: 'Unable to delete item.', type: 'danger' }, status: :unprocessable_entity
    end
  end

  def search
    @items = Item.joins(:categories)
      .where(
        'UPPER(title) LIKE :query OR categories.name LIKE :query',
        query: "%#{params[:query].upcase}%"
      )
      .limit(10)
  end

  private

  def item_params
    params.require(:item).permit(:title, :price, :sale_price, :description)
  end

  def item_choices
    choices = {}
    params[:item][:distinctions].each do |distinction|
      choices[distinction] = params[:item][distinction]
    end

    choices
  end

  def category_counts
    counts = { 'ALL' => params[:query] ? @items.count : Item.count }
    @items.each do |item|
      item.item_categories.each do |item_category|
        category = item_category.category.name
        counts[category] = counts[category] ? counts[category] + 1 : 1
      end
    end

    counts
  end
end
