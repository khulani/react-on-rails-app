class SessionsController < ApplicationController
  layout "login"
  skip_before_action :require_login, only: [:new, :create]

  def new
  end

  def create
    Rails.logger.info "#{params[:email]} #{params[:password]}"
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      if session[:source]
        redirect_to session[:source], notice: { message: 'Welcome.', type: 'success' }
      else
        redirect_to root_url, notice: { message: 'Welcome.', type: 'success' }
      end
    else
      flash.now[:alert] = "Email or password is invalid"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    if session[:source]
      redirect_to session[:source], notice: { message: 'Logged out.', type: 'success' }
    else
      redirect_to root_url, notice: { message: 'Logged out.', type: 'success' }
    end
  end
end
