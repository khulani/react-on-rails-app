class OrdersController < ApplicationController
  skip_before_action :require_login

  def index
    if params[:customer]
      @orders = Order.includes(:payment, :address, :order_items, :items)
        .where('orders.status != ?', 'cart')
        .where(items: { user_id: current_user.id }).order(created_at: :desc)
    else
      @orders = current_user.orders
        .includes(:payment, :address, :order_items, :items)
        .where('orders.status != ?', 'cart').order(created_at: :desc)
    end
  end

  def show
    if params[:customer]
      @order = Order.includes(:order_items, :items)
        .where(id: params[:id], items: { user_id: current_user.id }).first
    else
      @order = current_user.orders.find_by(id: params[:id])
    end
  end

  def cart
    @cart = Order.find_by(status: 'cart', anonymous: @anonymous)
  end

  def purchase
    order = Order.find_by(status: 'cart', anonymous: @anonymous)

    order.purchase(purchase_params)
    render json: { order_id: order.uuid }, status: :ok
  end

  private

  def address_params
    params.require(:address).permit(:full_name, :street1, :city, :state, :zip_code)
  end

  def payment_params
    params.require(:payment).permit(:card, :card_type, :token)
  end

  def purchase_params
    if params[:address_id]
      address_id = params[:address_id]
    else
      address_id = Address.create(address_params).id
    end

    details = {
      address_id: address_id,
      payment: payment_params,
      email: params[:email]
    }

    details[:user_id] = current_user.id if current_user

    details
  end

end
