class UsersController < ApplicationController
  skip_forgery_protection
  skip_before_action :require_login, only: [:new, :create]
  layout 'login'

  # GET /users
  # GET /users.json
  # def index
  #   @users = User.all
  # end

  # GET /users/1
  # GET /users/1.json
  # def show
  # end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  # def edit
  # end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        session[:user_id] = @user.id
        format.html { redirect_to root_path, notice: 'Account created. Welcome!' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @user = User.find(params[:id])
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    # current_user.avatar = params[:avatar] if params[:avatar]
    if params[:user][:old_password] && !current_user.authenticate(params[:user][:old_password])
      render json: { message: 'Old password invalid.', type: 'warning' }, status: :unprocessable_entity
    elsif current_user.update(user_params)
      render 'index.json'
    else
      render json: current_user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  # def destroy
  #   @user.destroy
  #   respond_to do |format|
  #     format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  def stripe
    begin
      res = RestClient.post(
        'https://connect.stripe.com/oauth/token',
        {
          client_secret: 'sk_test_klxrAxE5Td5GF7QJX1KS8jM0',
          code: params[:code],
          grant_type: 'authorization_code'
        }
      )
      data = JSON.parse(res.body)
      if data['stripe_user_id']
        current_user.update stripe_user_id: data['stripe_user_id']
        redirect_to '/user/profile', notice: { message: 'Stripe connected.', type: 'success'}
      else
        redirect_to '/user/profile', notice: { message: 'Unable to connect Stripe.', type: 'danger'}
      end
    rescue RestClient::ExceptionWithResponse => e
      error = JSON.parse(e.response)['error_description']
      redirect_to '/user/profile', notice: { message: error, type: 'danger'}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  # def set_user
  #   @user = User.find(params[:id])
  # end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(
      :email,
      :password,
      :password_confirmation,
      :first_name,
      :last_name,
      :stripe_user_id,
      :avatar
    )
 end
end
