class DistinctionsController < ApplicationController
  skip_before_action :require_login, only: [:index, :show]

  def index
    @distinctions = Distinction.where('name LIKE ?', "%#{params[:query]&.upcase}%")
  end
end
