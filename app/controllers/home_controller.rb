class HomeController < ApplicationController
  skip_before_action :require_login, except: :user

  def index
    session[:source] = nil
  end

  def order
    cart = Order.find_by(status: 'cart', anonymous: @anonymous)
    @cart_size = cart && cart.order_items.count || 0
  end
end
