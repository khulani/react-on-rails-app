class OrderItemsController < ApplicationController
  skip_before_action :require_login

  def index
    order = Order.find_by(id: params[:order_id])
    @order_items = order ? order.order_items : OrderItem.none
  end

  def create
    shopping_cart = Order.find_or_create_by(status: 'cart', anonymous: @anonymous)

    order_item = OrderItem.find_or_initialize_by(
      item_id: params[:order_item][:item_id],
      order: shopping_cart
    )

    order_item.assign_attributes(order_item_params)
    order_item.selection = params[:order_item][:selection]

    if order_item.save
      render json: { message: 'Item stored.', type: 'success' }, status: :ok
    else
      render json: {
        message: 'Unable to store item.',
        type: 'danger',
        error: order_item.errors
      }, status: :unprocessable_entity
    end
  end

  def update
    order_item = OrderItem.find_by(id: params[:id])

    if order_item&.update order_item_params
      render json: { message: 'Order item updated.', type: 'success' }, status: :ok
    else
      render json: {
        message: 'Unable to update order item.',
        type: 'danger',
        error: order_item.errors
      }, status: :unprocessable_entity
    end
  end

  def destroy
    shopping_cart = Order.find_by(status: 'cart', anonymous: @anonymous)

    order_item = OrderItem.find_by(item_id: params[:id], order: shopping_cart)

    if order_item&.destroy
      render json: { message: 'Order item removed.' }, status: :ok
    else
      render json: { message: 'Unable to remove order item.' }, status: :unprocessable_entity
    end
  end

  private

  def order_item_params
    params.require(:order_item).permit(:qty, :item_id, :status)
  end
end
