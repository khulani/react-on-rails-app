json.extract! user, :id, :first_name, :last_name, :email, :created_at
json.avatar user.avatar_url

json.stripe_user_id user.stripe_user_id if user == current_user
