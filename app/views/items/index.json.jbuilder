json.items do
  json.array! @items, partial: 'item', as: :item
end
json.pages @pages
json.categories @categories
