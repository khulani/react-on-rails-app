json.extract! item, :id, :title
json.price item.sale_price || item.price
json.photo_url item.photos.present? ? item.photos[0].photo_url : nil
