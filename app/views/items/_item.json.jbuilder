json.extract! item, :id, :title, :description, :price, :sale_price
json.user item.user.name
json.categories item.categories.map &:name
json.distinctions item.distinctions.map &:name

item.distinctions.each do |distinction|
  json.set! distinction.name do
    json.array! item.choices_by_distinction(distinction.name).pluck(:name)
  end
end

json.photos item.photos.map { |photo| { id: photo.id, photo_url: photo.photo_url } }
