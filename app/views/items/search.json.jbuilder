json.items @items do |item|
  json.extract! item, :id, :title, :description, :price, :sale_price
  json.user item.user.name
  json.categories item.categories.map &:name
  json.photo_url item.photos.first&.photo_url
end
