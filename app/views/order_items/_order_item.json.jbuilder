json.extract! order_item, :id, :qty, :selection, :price_paid, :status
json.item do
  json.partial! 'items/cart_item', item: order_item.item
end
