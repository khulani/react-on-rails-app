json.cart do
  if @cart
    json.extract! @cart, :id, :uuid
    json.order_items do
      @cart.order_items.each do |order_item|
        json.set! order_item.item.id do
          json.partial! 'order_items/order_item', order_item: order_item
        end
      end
    end
  else
    json.order_items Hash.new
  end
end
