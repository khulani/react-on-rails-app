if @order && @order.status != 'cart'
  json.partial! 'order', locals: { order: @order, order_items: true, payment: true }
end
