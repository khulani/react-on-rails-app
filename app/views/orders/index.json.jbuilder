json.orders @orders.each do |order|
  json.partial! 'order', locals: { order: order, order_items: false }
end
