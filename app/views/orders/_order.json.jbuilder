json.extract! order, :id, :uuid, :status, :email_address
json.total "#{order.total / 100.0}"

json.payment do
  json.extract! order.payment, :card, :card_type
  json.created_at order.payment.created_at.strftime("%Y-%m-%d")
end

json.address do
  json.extract! order.address, :full_name, :street1, :city, :state, :zip_code
end

if order_items
  json.order_items do
    order.order_items.each do |order_item|
      next unless order.item_ids.include? order_item.item_id
      json.set! order_item.item.id do
        json.partial! 'order_items/order_item', order_item: order_item
      end
    end
  end
end
