class Distinction < ApplicationRecord
  has_many :item_distinctions, dependent: :destroy
end
