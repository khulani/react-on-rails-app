class Choice < ApplicationRecord
  has_many :item_choices, dependent: :destroy
end
