class User < ApplicationRecord
  has_secure_password

  has_many :items, dependent: :destroy
  has_many :addresses, dependent: :destroy
  has_many :orders

  has_many :item_categories, through: :items
  has_many :categories, through: :item_categories

  mount_uploader :avatar, AvatarUploader

  validates :email, presence: true, uniqueness: true

  def name
    if first_name && last_name
      "#{first_name} #{last_name}"
    else
      email.split('@').first
    end
  end
end
