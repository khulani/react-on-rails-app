class ItemDistinction < ApplicationRecord
  belongs_to :item
  belongs_to :distinction
end
