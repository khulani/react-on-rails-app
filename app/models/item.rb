class Item < ApplicationRecord
  belongs_to :user

  has_many :item_categories, dependent: :destroy
  has_many :categories, through: :item_categories

  has_many :photos

  has_many :item_distinctions, dependent: :destroy
  has_many :distinctions, through: :item_distinctions

  has_many :item_choices, dependent: :destroy
  has_many :choices, through: :item_choices

  def update_categories(names)
    ids = []
    names.each do |name|
      ids << Category.find_or_create_by(name: name.upcase).id
    end

    self.category_ids = ids
  end

  def update_distinctions(names)
    ids = []
    names.each do |name|
      ids << Distinction.find_or_create_by(name: name.upcase).id
    end

    self.distinction_ids = ids
  end

  def update_choices(values)
    ids = []
    values.each do |distinction, names|
      names.each do |name|
        ids << Choice.find_or_create_by(name: name.upcase, distinction: distinction.upcase).id
      end
    end

    self.choice_ids = ids
  end

  def choices_by_distinction(distinction)
    choices.where(distinction: distinction)
  end

  def current_price
    sale_price || price
  end

  def props
    data = {
      id: id,
      title: title,
      price: price,
      sale_price: sale_price,
      description: description,
      user: user.name,
      categories: categories.map(&:name),
      distinctions: distinctions.map(&:name),
      photos: photos.map { |photo| { id: photo.id, photo_url: photo.photo_url } }
    }
    distinctions.each do |distinction|
      data[distinction.name] = choices_by_distinction(distinction.name).map(&:name)
    end

    data
  end
end
