class Order < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :address, optional: true

  has_many :order_items
  has_many :items, through: :order_items

  has_one :payment, dependent: :destroy

  before_save :ensure_uuid

  def purchase details
    self.status = 'submitted'
    order_items.each do |order_item|
      order_item.update(
        status: 'pending',
        price_paid: order_item.item.current_price
      )
    end

    self.address_id = details[:address_id]
    self.user_id = details[:user_id]
    self.email_address = details[:email]

    charge = Stripe::Charge.create(
      amount: total,
      currency: 'usd',
      source: details[:payment][:token],
      transfer_group: uuid
    )

    self.payment = Payment.create(
      card: details[:payment][:card],
      card_type: details[:payment][:card_type],
      charge_id: charge['id'],
      order_id: id
    )

    save
  end

  def status
    order_status = ''

    order_items.each do |order_item|
      # total based on pre-loaded items
      next unless item_ids.include?(order_item.item_id)

      if order_status == 'submitted' && order_item.completed? ||
          order_status == 'completed' && order_item.pending? ||
          !(order_item.completed? || order_item.pending?)
        order_status = 'in progress'
        break
      end
      order_status = order_item.completed? ? 'completed' : 'submitted'
    end

    order_status
  end

  def total
    total_cents = 0

    order_items.each do |order_item|
      # total based on pre-loaded items
      next unless order_item.paid? && item_ids.include?(order_item.item_id)

      total_cents += order_item.qty.to_i * order_item.price * 100
    end

    total_cents.to_i
  end

  def order_total
    total_price = 0
    order_items.each do |order_item|
      total_price += order_item.qty * order_item.price
    end

    total_price
  end

  private

  def ensure_uuid
    self.uuid ||= custom_uuid
  end

  def custom_uuid
    "#{SecureRandom.hex(2)}-#{SecureRandom.hex(2)}-#{SecureRandom.hex(4)}"
  end
end
