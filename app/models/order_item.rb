class OrderItem < ApplicationRecord
  WEBSITE_FEE_PCT = 0.05

  belongs_to :order
  belongs_to :item

  def price
    price_paid || item.current_price
  end

  def paid?
    status != 'cancelled'
  end

  def completed?
    status == 'delivered' || status == 'cancelled'
  end

  def pending?
    status == 'pending'
  end

  def status= (value)
    return if status == 'cancelled'

    if status == 'pending' && value == 'accepted'
      accept_transfer
    elsif value == 'cancelled'
      return_transfer if status != 'pending'
      refund_payment
    end

    super(value)
  end

  private

  def accept_transfer
    transfer = Stripe::Transfer.create(
      amount: transfer_amount,
      currency: "usd",
      destination: item.user.stripe_user_id,
      transfer_group: order.uuid
    )

    self.transfer_id = transfer['id']
  end

  def return_transfer
    transfer = Stripe::Transfer.retrieve(transfer_id)
    transfer.reversals.create amount: transfer_amount
  end

  def transfer_amount
    ((price_paid * qty * (0.971 - WEBSITE_FEE_PCT) +
      -0.3 * (price_paid / order.order_total)) * 100).to_i
  end

  def refund_payment
    Stripe::Refund.create(
      charge: order.payment.charge_id,
      amount: (price_paid * qty * 100).to_i
    )
  end
end
