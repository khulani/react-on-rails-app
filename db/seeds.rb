# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.destroy_all
Category.destroy_all
Item.destroy_all
Distinction.destroy_all
Choice.destroy_all

user = User.create email: 'khulani@gmail.com', password: 'khulani', first_name: 'khulani', last_name: 'malone'

shirt = Category.create name: 'shirts'
Category.create name: 'shoes'
Category.create name: 'pants'
Category.create name: 'hats'


color = Distinction.create name: 'color'
size = Distinction.create name: 'size'

purple = Choice.create distinction: 'color', name: 'purple'
yellow = Choice.create distinction: 'color', name: 'yellow'
green = Choice.create distinction: 'color', name: 'green'
Choice.create distinction: 'color', name: 'pink'

small = Choice.create distinction: 'size', name: 'small'
medium = Choice.create distinction: 'size', name: 'medium'
large = Choice.create distinction: 'size', name: 'large'
Choice.create distinction: 'size', name: 'x-large'

item = Item.create user: user, title: 'Khu T-shirt', price: 9.99, sale_price: 3.99, description: 'T-shirt made by khu'
item.categories << shirt
item.distinctions << color
item.choices << purple
item.choices << green
item.distinctions << size
item.choices << small
item.choices << medium
