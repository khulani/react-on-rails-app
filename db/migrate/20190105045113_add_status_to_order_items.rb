class AddStatusToOrderItems < ActiveRecord::Migration[5.2]
  def change
    add_column :order_items, :status, :string
    add_column :order_items, :transfer_id, :string
    add_column :order_items, :price_paid, :decimal, precision: 8, scale: 2
  end
end
