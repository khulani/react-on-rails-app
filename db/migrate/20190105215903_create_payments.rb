class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.string :card
      t.string :card_type
      t.string :charge_id
      t.references :order

      t.timestamps
    end
  end
end
