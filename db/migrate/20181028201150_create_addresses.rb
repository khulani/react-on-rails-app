class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :street1, null: false
      t.string :street2
      t.string :city, null: false
      t.string :state, null: false
      t.string :zip_code, null: false
      t.string :country
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
