class CreateChoices < ActiveRecord::Migration[5.2]
  def change
    create_table :choices do |t|
      t.string :name
      t.string :distinction

      t.timestamps
    end

    add_index :choices, :name
    add_index :choices, :distinction
  end
end
