class AddFullNameToAddresses < ActiveRecord::Migration[5.2]
  def change
    add_column :addresses, :full_name, :string
    remove_foreign_key :addresses, :users
  end
end
