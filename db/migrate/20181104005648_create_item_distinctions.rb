class CreateItemDistinctions < ActiveRecord::Migration[5.2]
  def change
    create_table :item_distinctions do |t|
      t.references :distinction, foreign_key: true
      t.references :item, foreign_key: true

      t.timestamps
    end
  end
end
