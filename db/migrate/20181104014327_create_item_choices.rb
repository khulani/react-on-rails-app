class CreateItemChoices < ActiveRecord::Migration[5.2]
  def change
    create_table :item_choices do |t|
      t.references :choice, foreign_key: true
      t.references :item, foreign_key: true

      t.timestamps
    end
  end
end
