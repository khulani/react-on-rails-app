class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :title, null: false
      t.decimal :price, precision: 8, scale: 2
      t.decimal :sale_price, precision: 8, scale: 2
      t.integer :quantity
      t.string :description
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
