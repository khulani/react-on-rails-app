class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :status
      t.string :email_address
      t.string :anonymous
      t.references :user
      t.references :address

      t.timestamps
    end

    add_index :orders, :status
  end
end
