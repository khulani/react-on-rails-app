class CreateDistinctions < ActiveRecord::Migration[5.2]
  def change
    create_table :distinctions do |t|
      t.string :name

      t.timestamps
    end

    add_index :distinctions, :name, unique: true
  end
end
